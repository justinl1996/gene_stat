#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

#include "test_maternal.h"
//#include "test_expr_parser.h"

CPPUNIT_TEST_SUITE_REGISTRATION(TestMaternal);
//CPPUNIT_TEST_SUITE_REGISTRATION(TestExprParser);

int main(int argc, char **argv)
{
    CPPUNIT_NS::Test *suite = CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest();
    
    CppUnit::TextUi::TestRunner runner;
    runner.addTest(suite);
    bool wasSucessful = runner.run();
    
    return wasSucessful ? 0 : 1;
}

