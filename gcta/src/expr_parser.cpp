#include <iostream>
#include <fstream>
#include <queue>
#include <unordered_map>

#include "gcta.h"
#include "StringOps.h"

ExprParserProb::ExprParserProb()
{
    //symbol_table = new symbol_table_t;
    symbol_table.add_variable("p", p);
    symbol_table.add_variable("q", q);
    
    // generate strings Pa .. Pz, P0 -> LHS variables
    /*for (int i = 0; i < 27; i++) {
        eval_result.push_back(0.0f);
        string var;
        //if (i < 10) {
        //    var = "P" + StringOps::intToStr(i);
        //} else {
        if (i < 26) {
            string suffix(1, 'a' + i);
            var = "P" + suffix; 
        } else {
            var = "P" + StringOps::intToStr(i-26);
        }
        symbol_table.add_variable(var, eval_result[i-1]);
        prob_exp.push_back(var);
    }*/
    vector<string> all_expr(full_siblings_expr);
    all_expr.insert(all_expr.end(), half_siblings_expr.begin(), half_siblings_expr.end());
    all_expr.insert(all_expr.end(), full_siblings_X_expr_male_female.begin(), full_siblings_X_expr_male_female.end());
    all_expr.insert(all_expr.end(), full_siblings_X_expr_female_female.begin(), full_siblings_X_expr_female_female.end());
    all_expr.insert(all_expr.end(), full_siblings_X_expr_male_male.begin(), full_siblings_X_expr_male_male.end());
    all_expr.insert(all_expr.end(), half_siblings_X_expr_female_female.begin(), half_siblings_X_expr_female_female.end());
    all_expr.insert(all_expr.end(), half_siblings_X_expr_male_male.begin(), half_siblings_X_expr_male_male.end());
    all_expr.insert(all_expr.end(), half_siblings_X_expr_male_female.begin(), half_siblings_X_expr_male_female.end());

    for (int i = 0; i < all_expr.size(); i++)
    {
        eval_result.push_back(0.0f);
        string var = all_expr[i];//full_siblings_expr[i];

        symbol_table.add_variable(var, eval_result[i-1]);
        prob_exp.push_back(var);
    }

    // add expressions to symbol table
    for (vector<string>::const_iterator it = prob_exp.begin(); 
        it != prob_exp.end(); it++)
    {
        expression_t expression;
        expression.register_symbol_table(symbol_table);
        prob_exp_map[*it] = std::unique_ptr<ExpNode>(new ExpNode(*it, expression));
        //cout << *it << endl;
    }
    correct = false;

    //symbol_table.add_variable("y",y);
    //symbol_table.add_variable("z",z);
}

/**
 * Parses a file with user defined probabilities, checks for errors and 
 * resolves dependencies between expressions
 */
void ExprParserProb::parse_file(const string &filename)
{
    ifstream infile(filename);
    string line;
    unsigned line_count = 0, found_count = 0;

    if (!infile.is_open())
        throw ("Error opening file " + filename + " for reading");

    //cout << "##################" << endl;
    std::vector<bool> found_exp(prob_exp_map.size(), false);
    
    while ( getline (infile,line) )
    {
        line_count++;
        line = StringOps::trim(line);

        //skip empty line or comment 
        if (line.size() == 0 || line[0] == '#') {
            continue;
        }
        std::size_t sep = line.find_first_of("=");
        if (sep == string::npos) {
            break;
        }
        string lhs = StringOps::trim(line.substr(0, sep - 1));
        string rhs = StringOps::trim(line.substr(sep + 1));
    
        cout << lhs << endl;

        //cout << "finding:'" << lhs << "' " << rhs << endl;
        for (vector<string>::const_iterator it = prob_exp.begin(); 
            it != prob_exp.end(); it++)
        {
            if (lhs == *it) {
                //already found rhs (means we have to evaluate it first so make that the parent)
                if (prob_exp_map.find(rhs) != prob_exp_map.end()) {
                    prob_exp_map[*it]->set_parent(prob_exp_map[rhs].get()); 
                } else {
                    prob_exp_map[*it]->set_expression_string(rhs);
                }
                
                // mark as being found
                unsigned index = it - prob_exp.begin();  
                found_exp[index] = true;
                found_count++;
                break;
            }
        }
    }
    
    infile.close();

    // we haven't got enough expressions, thow an exception and quit   
    if (found_count != prob_exp.size()) 
    {
        stringstream ss;
        ss << "Missing probability equation(s): " << endl;
        for (int i = 0; i < found_exp.size(); i++)
        {
            if (found_exp[i] == false) {
                ss << prob_exp[i] << endl;
            }    
        }         
        throw(ss.str());
    }
    
    this->resolve_dependencies();
    this->output_expressions();
    correct = true;
}

/**
 * Resolve RHS dependency
 */
void ExprParserProb::resolve_dependencies()
{
    set<string> seen;

    for (var_exp_t::iterator it = prob_exp_map.begin(); 
        it != prob_exp_map.end(); it++)
    {
        // its been evaluated before
        if (seen.find(it->first) != seen.end()) {
            continue;
        }

        // has no dependency (is a root) 
        if (it->second->get_parent() == NULL) {
            seen.insert(it->first);
            //cout << it->first << endl;
            //cout << it->second->get_expression_string() << endl;
            it->second->parse();
        } else {
            stack<string> dependency;
            dependency.push(it->first);
            ExpNode *next = it->second->get_parent(); 
            string expr_str = "";

            //follow links back to roott
            while (next != NULL) {
                if (seen.find(next->get_key()) != seen.end()) {
                    // hit a node we have already evaluated
                    expr_str = next->get_expression_string();
                    break;
                } else if (next->get_key() == it->first) {
                    // get back to the beginning this is bad  
                    throw("Circular dependency: " + it->first);
                }
                dependency.push(next->get_key());
                expr_str = next->get_expression_string();
                next = next->get_parent();
            }

            // copy expression strings and parse
            while (!dependency.empty())
            {
                string id = dependency.top();
                seen.insert(id);
                prob_exp_map[id]->set_expression_string(expr_str);
                prob_exp_map[id]->parse();
                dependency.pop();
            }           
        }
    }
}

/**
 * Output the all expressions in human readable form 
 */
void ExprParserProb::output_expressions()
{
    Logger::getInstance() << "Found the following equations:" << endl;
    
    for (vector<string>::const_iterator it = prob_exp.begin(); it != prob_exp.end(); it++)
    {
        Logger::getInstance() << *it << " = " << prob_exp_map[*it]->get_expression_string() << endl;
    }
    Logger::getInstance() << endl;
}

/**
 * Evaluate the expression given the expression id
 */ 
float ExprParserProb::get_probability(const string& id)
{
    return prob_exp_map[id]->eval();
} 

/**
 * Evaluate the expression given a list of expression ids
 */
std::vector<float> ExprParserProb::get_probabilities(vector<string> &ids)
{
    std::vector<float> result;
    for (int i = 0; i < ids.size(); i++)
    {
        if (ids[i] == "")
            result.push_back(0);
        else
            result.push_back(prob_exp_map[ids[i]]->eval());
    }
    return result;
}

std::vector<float> ExprParserProb::test(float _p, float _q)
{
    this->p = _p;
    this->q = _q;
    #if 1

    std::vector<float> results;
    results.resize(prob_exp.size());

    for (var_exp_t::iterator it = prob_exp_map.begin(); it != prob_exp_map.end(); it++)
    {
        float ans = it->second->eval();

        for (int i = 0; i < prob_exp.size(); i++) {
            if (it->first == prob_exp[i]) {
                results[i] = ans;
            }
        }
    }

    #endif

    return results;
}