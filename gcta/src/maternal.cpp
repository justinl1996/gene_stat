#include <unordered_map>
#include <unordered_set>
#include <set>
#include <math.h>
#include "StringOps.h"

#include "gcta.h"

/**
 * Return the sum of elements in a vector
 **/
template <typename T> T sum_vec(vector<T> vec)
{
    T result = 0;
    for (auto& n : vec)
        result += n;
    return result;
}

/**
 * Returns true if elem can be found in the vector
 **/
template <typename T> bool find_in_vector(vector<T> vec, T elem)
{
    typename vector<T>::iterator iter = std::find(vec.begin(), vec.end(), elem);
    return iter != vec.end();
}

/**
 * Add individual indices to parent_map ensuring no duplicates
 **/
void add_to_parent_map(parent_map_t &_parent_sib_map, const string &key, 
    const vector<int> &indices, SibRel rel)
{
    parent_map_t::iterator got = _parent_sib_map.find(key);
    
    if (got != _parent_sib_map.end()) {
        for (auto i = indices.begin(); i != indices.end(); i++)
        {
            // case where there are > 2 siblings 
            if (!find_in_vector(got->second.children, *i)) {
                got->second.children.push_back(*i);
            }
        }
    } else {
        _parent_sib_map[key] = {indices, {}, rel};
    }
}

/**
 * Check mother or father id is valid (ie. not -9 or 0). mother = True -> check for mother id
 * indi: individual index
 * mother: true if maternal else false
 **/
bool gcta::check_parental_id_valid(int indi, bool mother)
{
    if (mother) 
    {
        return !(_mo_id[indi] == "-9" || _mo_id[indi] == "0");
    }
    else
    {
        return !(_fa_id[indi] == "-9" || _fa_id[indi] == "0");
    }
}

/**
 * Check both mother and father id is valid (ie. not -9 or 0) 
 * indi: individual index
 **/
bool gcta::check_parental_id_valid(int indi)
{
    return check_parental_id_valid(indi, false) || check_parental_id_valid(indi, false); 
}

/**
 * Check gender is valid (i.e. 1 or 2) for individual with index indi
 **/
bool gcta::check_gender_valid(int indi)
{
    if (_sex[_keep[indi]] != 1 && _sex[_keep[indi]] != 2)
    {
        Logger::getInstance() << "Individual with fid:" + _fid[indi] + " and id:" + _pid[indi] 
            << " has invalid gender, cannot impute for Xchromosome" << endl;
        return false;
    } else {
        return true;
    }
}

/**
 * Returns genotype (AA, Aa or aa) for a snp index given an individual index p
 * snp_i: snp index
 * p: invidual index
 **/
enum SNPGenotype gcta::get_genotype(int snp_i, int p)
{
    int p_geno = 0;
    // 3 == 11 == AA 
    // 0 == 00 == aa 
    // 1 == 01 == Aa 
    // 2 == 10 == INVALID

    if (!_snp_1[snp_i][p]) 
        p_geno |= (1 << 0);        
    if (!_snp_2[snp_i][p]) 
        p_geno |= (1 << 1);

    return (enum SNPGenotype) p_geno; 
}

/** 
 * Computes parental probabilities for fullsibs from hard coded equations
 * Returns vector of length 3: P(Mum = AA| ...), P(Mum = Aa | ...), P(Mum = aa | ...)
 * p1_geno: individual 1 genotype: AA Aa or aa
 * p2_geno: individual 2 genotype: AA Aa or aa
 * maf: maximum allele frequency
 **/
vector<float> gcta::calc_parental_prob_full_sib_default(enum SNPGenotype p1_geno, 
    enum SNPGenotype p2_geno, float maf)
{
    vector<float> result;
    float p = maf;
    float q = 1 - maf;

    if (p1_geno == INVALID || p2_geno == INVALID) 
    {
        return result;
    }
    if (p1_geno == AA && p2_geno == AA) 
    {
        result = {(2*p)/(p+1), (1-p)/(1+p), 0};
    }  
    else if ((p1_geno == AA && p2_geno == Aa) || (p1_geno == Aa && p2_geno == AA))
    {
        result = {p/(p+1), 1/(p+1), 0}; 
    }
    else if ((p1_geno == AA && p2_geno == aa) || (p1_geno == aa && p2_geno == AA))
    {
        result = {0, 1, 0};   
    } 
    else if (p1_geno == Aa && p2_geno == Aa)
    {
        result = {(p*(1-0.5f*p))/(1+p-p*p), 0.5f/(1+p-p*p), 0.5f*(1-p*p)/(1+p-p*p)};
    } 
    else if ((p1_geno == Aa && p2_geno == aa) || (p1_geno == aa && p2_geno == Aa))
    {
        result = {0, 1/(q+1), q/(q+1)};
    } 
    else if ((p1_geno == aa && p2_geno == aa)) 
    {
        result = {0, (1-q)/(q+1), 2*q/(q+1)};
    } 
    else 
    {
        Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
    }
    return result;
}

/** 
 * Computes parental probabilities from user defined equations 
 * of half siblings xchromosome (same mother + father) 
 * p1_geno: individual 1 genotype: AA Aa or aa
 * p2_geno: individual 2 genotype: AA Aa or aa
 * p1_sex: gender of individual 1
 * p2_sex: gender of individual 2
 * maf: maximum allele frequency
 * same_mother: true if siblings share the same mother
 **/
pair<vector<float>, vector<float>> gcta::calc_prob_half_sib_Xchr(
    enum SNPGenotype p1_geno, enum SNPGenotype p2_geno, enum Gender p1_sex, enum Gender p2_sex, 
    float maf, bool same_mother)
{
    vector<float> mat_result, pat_result;
    vector<string> mat_ids, pat_ids;
    _exp_parser.set_p(maf);
    _exp_parser.set_q(1-maf);    
    
    if (same_mother) 
    {
        if (p1_sex == MALE && p2_sex == MALE) 
        {
            if (p1_geno == AA && p2_geno == AA) 
            {
                mat_ids = {"PXhm_mAA_mA_mA","PXhm_mAa_mA_mA","PXhm_maa_mA_mA"};
            }
            else if (p1_geno == AA && p2_geno == aa)
            {
                mat_ids = {"PXhm_mAA_mA_ma","PXhm_mAa_mA_ma","PXhm_maa_mA_ma"};
            }
            else if (p1_geno == aa && p2_geno == AA)
            {
                mat_ids = {"PXhm_mAA_ma_mA","PXhm_mAa_ma_mA","PXhm_maa_ma_mA"};
            }
            else if (p1_geno == aa && p2_geno == aa)
            {
                mat_ids = {"PXhm_mAA_ma_ma","PXhm_mAa_ma_ma","PXhm_maa_ma_ma"};
            } 
            else
            {
                Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
            }

            pat_ids = {"", "", ""};
        }
        else if (p1_sex == FEMALE && p2_sex == FEMALE)
        {
            if (p1_geno == AA && p2_geno == AA) 
            {
                mat_ids = {"PXhm_mAA_fAA_fAA","PXhm_mAa_fAA_fAA","PXhm_maa_fAA_fAA"};
                pat_ids = {"PXhm_fAA_fAA_fAA","","PXhm_faa_fAA_fAA"};
            }
            else if (p1_geno == AA && p2_geno == Aa)
            {
                mat_ids = {"PXhm_mAA_fAA_fAa","PXhm_mAa_fAA_fAa","PXhm_maa_fAA_fAa"};
                pat_ids = {"PXhm_fAA_fAA_fAa","","PXhm_faa_fAA_fAa"};
            }
            else if (p1_geno == AA && p2_geno == aa)
            {
                mat_ids = {"PXhm_mAA_fAA_faa","PXhm_mAa_fAA_faa","PXhm_maa_fAA_faa"};
                pat_ids = {"PXhm_fAA_fAA_faa","","PXhm_faa_fAA_faa"};
            }
            else if (p1_geno == Aa && p2_geno == AA)
            {
                mat_ids = {"PXhm_mAA_fAa_fAA","PXhm_mAa_fAa_fAA","PXhm_maa_fAa_fAA"};
                pat_ids = {"PXhm_fAA_fAa_fAA","","PXhm_faa_fAa_fAA"};
            } 
            else if (p1_geno == Aa && p2_geno == Aa)
            {
                //mat_result = {2*p*(1-p)/(1+4*p-4*p*p), 1.0f/(1+4*p-4*p*p),  2*p*(1-p)/(1+4*p-4*p*p)};
                //pat_result = {p*(2-p)/(1+4*p-4*p*p), 0, q*(2-q)/(1+4*q-4*q*q)};
                // {P(D1=A,D2=A|...) + 0 + P(D1=A,D2=aa|...), 0, P(D1=aa,D2=AA|...) + 0 + P(D1=aa,D2=aa|...)
                /*pat_result = 
                { 
                    (p*(2-p))/(1+4*p-4*p*p) + (p*(1-p))/(1+4*p-4*p*p), 0,  
                    (q*(1-q))/(1+4*q-4*q*q) + (q*(2-q))/(1+4*q-4*q*q)
                };*/
                mat_ids = {"PXhm_mAA_fAa_fAa","PXhm_mAa_fAa_fAa","PXhm_maa_fAa_fAa"};
                pat_ids = {"PXhm_fAA_fAA_fAa_fAa","PXhm_fAA_faa_fAa_fAa","PXhm_faa_fAA_fAa_fAa","PXhm_faa_faa_fAa_fAa"};

                vector<float> temp = _exp_parser.get_probabilities(pat_ids);
                mat_result = _exp_parser.get_probabilities(mat_ids); 
                pat_result = {temp[0] + temp[1], 0, temp[2] + temp[3]}; 

                return make_pair(mat_result, pat_result);
            }
            else if (p1_geno == Aa && p2_geno == aa)
            {
                mat_ids = {"PXhm_mAA_fAa_faa","PXhm_mAa_fAa_faa","PXhm_maa_fAa_faa"};
                pat_ids = {"PXhm_fAA_fAa_faa","","PXhm_faa_fAa_faa"};
            }
            else if (p1_geno == aa && p2_geno == AA) 
            {
                mat_ids = {"PXhm_mAA_faa_fAA","PXhm_mAa_faa_fAA","PXhm_maa_faa_fAA"};
                pat_ids = {"PXhm_fAA_faa_fAA","","PXhm_faa_faa_fAA"};
            }
            else if (p1_geno == aa && p2_geno == Aa) 
            {
                mat_ids = {"PXhm_mAA_faa_fAa","PXhm_mAa_faa_fAa","PXhm_maa_faa_fAa"};
                pat_ids = {"PXhm_fAA_faa_fAa","","PXhm_faa_faa_fAa"};
            }
            else if (p1_geno == aa && p2_geno == aa)
            {
                mat_ids = {"PXhm_mAA_faa_faa","PXhm_mAa_faa_faa","PXhm_maa_faa_faa"};
                pat_ids = {"PXhm_fAA_faa_faa","","PXhm_faa_faa_faa"};
            }    
            else 
            {
                Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
            }            
        }
        else
        {
            if (p1_sex == FEMALE && p2_sex == MALE)
            {
                swap(p1_sex, p2_sex);
                swap(p1_geno, p2_geno);
            }
            //p1=male, p2=female
            if (p1_geno == AA && p2_geno == AA)
            {
                mat_ids = {"PXhm_mAA_mA_fAA","PXhm_mAa_mA_fAA","PXhm_maa_mA_fAA"};
                pat_ids = {"PXhm_fAA_mA_fAA","","PXhm_faa_mA_fAA"};
            }
            else if (p1_geno == AA && p2_geno == Aa)
            {
                mat_ids = {"PXhm_mAA_mA_fAa","PXhm_mAa_mA_fAa","PXhm_maa_mA_fAa"};
                pat_ids = {"PXhm_fAA_mA_fAa","","PXhm_faa_mA_fAa"};
            }
            else if (p1_geno == AA && p2_geno == aa)
            {
                mat_ids = {"PXhm_mAA_mA_faa","PXhm_mAa_mA_faa","PXhm_maa_mA_faa"};
                pat_ids = {"PXhm_fAA_mA_faa","","PXhm_faa_mA_faa"};
            }
            else if (p1_geno == aa && p2_geno == AA)
            {
                mat_ids = {"PXhm_mAA_ma_fAA","PXhm_mAa_ma_fAA","PXhm_maa_ma_fAA"};
                pat_ids = {"PXhm_fAA_ma_fAA","","PXhm_faa_ma_fAA"};
            }
            else if (p1_geno == aa && p2_geno == Aa)
            {
                mat_ids = {"PXhm_mAA_ma_fAa","PXhm_mAa_ma_fAa","PXhm_maa_ma_fAa"};
                pat_ids = {"PXhm_fAA_ma_fAa","","PXhm_faa_ma_fAa"};
            }
            else if (p1_geno == aa && p2_geno == aa)
            {
                mat_ids = {"PXhm_mAA_ma_faa","PXhm_mAa_ma_faa","PXhm_maa_ma_faa"};
                pat_ids = {"PXhm_fAA_ma_faa","","PXhm_faa_ma_faa"};
            }
            else 
            {
                Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
            }
        }
    }
    else
    {
        // shared father
        if (p1_sex == FEMALE && p2_sex == FEMALE)
        {
            if (p1_geno == AA && p2_geno == AA) 
            {
                mat_ids = {"PXhf_mAA_fAA_fAA","PXhf_mAa_fAA_fAA","PXhf_maa_fAA_fAA"};
                pat_ids = {"PXhf_fAA_fAA_fAA","","PXhf_faa_fAA_fAA"};
            }
            else if (p1_geno == AA && p2_geno == Aa) 
            {
                mat_ids = {"PXhf_mAA_fAA_fAa","PXhf_mAa_fAA_fAa","PXhf_maa_fAA_fAa"};
                pat_ids = {"PXhf_fAA_fAA_fAa","","PXhf_faa_fAA_fAa"};
            }
            else if (p1_geno == AA && p2_geno == aa) 
            {
                mat_ids = {"PXhf_mAA_fAA_faa","PXhf_mAa_fAA_faa","PXhf_maa_fAA_faa"};
                pat_ids = {"PXhf_fAA_fAA_faa","","PXhf_faa_fAA_faa"};
            }
            else if (p1_geno == Aa && p2_geno == AA)
            {                
                mat_ids = {"PXhf_mAA_fAa_fAA","PXhf_mAa_fAa_fAA","PXhf_maa_fAa_fAA"};
                pat_ids = {"PXhf_fAA_fAa_fAA","","PXhf_faa_fAa_fAA"};
            }
            else if (p1_geno == Aa && p2_geno == Aa)
            {
                mat_ids = {"PXhf_mAA_fAa_fAa","PXhf_mAa_fAa_fAa","PXhf_maa_fAa_fAa"};
                pat_ids = {"PXhf_fAA_fAa_fAa","","PXhf_faa_fAa_fAa"};
            }
            else if (p1_geno == Aa && p2_geno == aa)
            {
                mat_ids = {"PXhf_mAA_fAa_faa","PXhf_mAa_fAa_faa","PXhf_maa_fAa_faa"};
                pat_ids = {"PXhf_fAA_fAa_faa","","PXhf_faa_fAa_faa"};
            }
            else if (p1_geno == aa && p2_geno == AA)
            {
                mat_ids = {"PXhf_mAA_faa_fAA","PXhf_mAa_faa_fAA","PXhf_maa_faa_fAA"};
                pat_ids = {"PXhf_fAA_faa_fAA","","PXhf_faa_faa_fAA"};
            }
            else if (p1_geno == aa && p2_geno == Aa)
            {
                mat_ids = {"PXhf_mAA_faa_fAa","PXhf_mAa_faa_fAa","PXhf_maa_faa_fAa"};
                pat_ids = {"PXhf_fAA_faa_fAa","","PXhf_faa_faa_fAa"};
            }
            else if (p1_geno == aa && p2_geno == aa)
            {
                mat_ids = {"PXhf_mAA_faa_faa","PXhf_mAa_faa_faa","PXhf_maa_faa_faa"};
                pat_ids = {"PXhf_fAA_faa_faa","","PXhf_faa_faa_faa"};
            }
            else 
            {
                Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
            }
        }
        else if (p1_sex == MALE && p2_sex == MALE)
        {
            mat_result = {0, 0, 0};
            pat_result = {0, 0, 0};
        }
        else
        {
            // p1=male, p2=female
            if (p1_sex == MALE && p2_sex == FEMALE) 
            {
                if (p1_geno == AA && p2_geno == AA) 
                {
                    mat_ids = {"PXhf_mAA_mA_fAA","PXhf_mAa_mA_fAA","PXhf_maa_mA_fAA"};
                    pat_ids = {"PXhf_fAA_mA_fAA","","PXhf_faa_mA_fAA"};
                }
                else if (p1_geno == AA && p2_geno == Aa)
                {
                    mat_ids = {"PXhf_mAA_mA_fAa","PXhf_mAa_mA_fAa","PXhf_maa_mA_fAa"};
                    pat_ids = {"PXhf_fAA_mA_fAa","","PXhf_faa_mA_fAa"};
                }
                else if (p1_geno == AA && p2_geno == aa)
                {
                    mat_ids = {"PXhf_mAA_mA_faa","PXhf_mAa_mA_faa","PXhf_maa_mA_faa"};
                    pat_ids = {"PXhf_fAA_mA_faa","","PXhf_faa_mA_faa"};
                }
                else if (p1_geno == aa && p2_geno == AA)
                {
                    mat_ids = {"PXhf_mAA_ma_fAA","PXhf_mAa_ma_fAA","PXhf_maa_ma_fAA"};
                    pat_ids = {"PXhf_fAA_ma_fAA","","PXhf_faa_ma_fAA"};
                }
                else if (p1_geno == aa && p2_geno == Aa)
                {
                    mat_ids = {"PXhf_mAA_ma_fAa","PXhf_mAa_ma_fAa","PXhf_maa_ma_fAa"};
                    pat_ids = {"PXhf_fAA_ma_fAa","","PXhf_faa_ma_fAa"};
                }
                else if (p1_geno == aa && p2_geno == aa)
                {
                    mat_ids = {"PXhf_mAA_faa_ma","PXhf_mAa_faa_ma","PXhf_maa_faa_ma"};
                    pat_ids = {"PXhf_fAA_ma_faa","","PXhf_faa_ma_faa"};
                }
                else 
                {
                    Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
                }
            }            
            else 
            {
                // p1=female, p2=male
                if ( p1_geno == AA && p2_geno == AA) 
                {
                    mat_ids = {"PXhf_mAA_fAA_mA","PXhf_mAa_fAA_mA","PXhf_maa_fAA_mA"};
                    pat_ids = {"PXhf_fAA_mA_fAA","","PXhf_faa_mA_fAA"};
                }
                else if (p1_geno == Aa && p2_geno == AA)
                {
                    mat_ids = {"PXhf_mAA_fAa_mA","PXhf_mAa_fAa_mA","PXhf_maa_fAa_mA"};
                    pat_ids = {"PXhf_fAA_mA_fAa","","PXhf_faa_mA_fAa"};
                }
                else if (p1_geno == aa && p2_geno == AA)
                {
                    mat_ids = {"PXhf_mAA_faa_mA","PXhf_mAa_faa_mA","PXhf_maa_faa_mA"};
                    pat_ids = {"PXhf_fAA_ma_fAA","","PXhf_faa_ma_fAA"};
                }
                else if (p1_geno == AA && p2_geno == aa)
                {
                    mat_ids = {"PXhf_mAA_fAA_ma","PXhf_mAa_fAA_ma","PXhf_maa_fAA_ma"};
                    pat_ids = {"PXhf_fAA_ma_fAA","","PXhf_faa_ma_fAA"};
                }
                else if (p1_geno == Aa && p2_geno == aa)
                {
                    mat_ids = {"PXhf_mAA_fAa_ma","PXhf_mAa_fAa_ma","PXhf_maa_fAa_ma"};
                    pat_ids = {"PXhf_fAA_ma_fAa","","PXhf_faa_ma_fAa"};
                }
                else if (p1_geno == aa && p2_geno == aa)
                {
                    mat_ids = {"PXhf_mAA_faa_ma","PXhf_mAa_faa_ma","PXhf_maa_faa_ma"};
                    pat_ids = {"PXhf_fAA_ma_faa","","PXhf_faa_ma_faa"};
                } 
                else 
                {
                    Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
                }
            }             
        }
    }
    mat_result = _exp_parser.get_probabilities(mat_ids);
    pat_result = _exp_parser.get_probabilities(pat_ids);
    return make_pair(mat_result, pat_result);
}


/** 
 * Computes parental probabilities for halfsibs from hard coded equations
 * Returns tuple with 2 vectors of length 3: P(Mum = AA| ...), P(Mum = Aa | ...), P(Mum = aa | ...)
 * corresponding to the mother and father
 * p1_geno: individual 1 genotype: AA Aa or aa
 * p2_geno: individual 2 genotype: AA Aa or aa
 * p1_sex: gender of individual 1
 * p2_sex: gender of individual 2
 * maf: maximum allele frequency
 * same_mother: true if siblings share the same mother
 */
pair<vector<float>, vector<float>> gcta::calc_prob_half_sib_Xchr_default(
    enum SNPGenotype p1_geno, enum SNPGenotype p2_geno, enum Gender p1_sex, enum Gender p2_sex, 
    float maf, bool same_mother)
{
    vector<float> mat_result, pat_result;
    float p = maf;
    float q = 1 - maf;

    //todo: break down if conditions into functions
    if (same_mother) 
    {
        if (p1_sex == MALE && p2_sex == MALE) 
        {
            pair<vector<float>, vector<float>> temp = 
                calc_prob_full_sib_Xchr_default(p1_geno, p2_geno, p1_sex, p2_sex, maf);
            mat_result = temp.first;
            pat_result = {0, 0, 0};
        } 
        else if (p1_sex == FEMALE && p2_sex == FEMALE)
        {
            if (p1_geno == AA && p2_geno == AA) 
            {
                mat_result = {2*p/(p+1), (1-p)/(p+1), 0};
                pat_result = {1, 0, 0};
            }
            else if (p1_geno == AA && p2_geno == Aa)
            {
                mat_result = {2*p/(2*p+1), 1/(2*p+1), 0};
                pat_result = {1, 0, 0};
            }
            else if (p1_geno == AA && p2_geno == aa)
            {
                mat_result = {0, 1, 0};
                pat_result = {1, 0, 0};
            }
            else if (p1_geno == Aa && p2_geno == AA)
            {
                mat_result = {2*p/(2*p+1), 1/(2*p+1), 0};
                pat_result = {p/(2*p+1), 0, (p+1)/(2*p+1)};
            } 
            else if (p1_geno == Aa && p2_geno == Aa)
            {
                mat_result = {2*p*(1-p)/(1+4*p-4*p*p), 1.0f/(1+4*p-4*p*p),  2*p*(1-p)/(1+4*p-4*p*p)};
                //pat_result = {p*(2-p)/(1+4*p-4*p*p), 0, q*(2-q)/(1+4*q-4*q*q)};
                // {P(D1=A,D2=A|...) + 0 + P(D1=A,D2=aa|...), 0, P(D1=aa,D2=AA|...) + 0 + P(D1=aa,D2=aa|...)
                pat_result = 
                { 
                    (p*(2-p))/(1+4*p-4*p*p) + (p*(1-p))/(1+4*p-4*p*p), 0,  
                    (q*(1-q))/(1+4*q-4*q*q) + (q*(2-q))/(1+4*q-4*q*q)
                };
            }
            else if (p1_geno == Aa && p2_geno == aa)
            {
                mat_result = {0, 1/(2*q+1), 2*q/(2*q+1)};
                pat_result = {(q+1)/(2*q+1), 0, q/(2*q+1)};
            }
            else if (p1_geno == aa && p2_geno == AA) 
            {
                mat_result = {0, 1, 0};
                pat_result = {0, 0, 1};
            }
            else if (p1_geno == aa && p2_geno == Aa) 
            {
                mat_result = {0, 1/(2*q+1), 2*q/(2*q+1)};
                pat_result = {0, 0, 1};
            }
            else if (p1_geno == aa && p2_geno == aa)
            {
                mat_result = {0, (1-q)/(q+1), 2*q/(q+1)};
                pat_result = {0, 0, 1};
            }    
            else 
            {
                Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
            }
        } 
        else 
        {
            if (p1_sex == FEMALE && p2_sex == MALE)
            {
                swap(p1_sex, p2_sex);
                swap(p1_geno, p2_geno);
            }
            //p1=male, p2=female
            if (p1_geno == AA && p2_geno == AA)
            {
                mat_result = {2*p/(p+1), (1-p)/(p+1), 0};
                pat_result = {1, 0, 0};
            }
            else if (p1_geno == AA && p2_geno == Aa)
            {
                mat_result = {2*p/(2*p+1), 1/(2*p+1), 0};
                pat_result = {p/(2*p+1), 0, (p+1)/(2*p+1)};
            }
            else if (p1_geno == AA && p2_geno == aa)
            {
                mat_result = {0, 1, 0};
                pat_result = {0, 0, 1};
            }
            else if (p1_geno == aa && p2_geno == AA)
            {
                mat_result = {0, 1, 0};
                pat_result = {1, 0, 0};
            }
            else if (p1_geno == aa && p2_geno == Aa)
            {
                mat_result = {0, 1/(2*q+1), 2*q/(2*q+1)};
                pat_result = {(q+1)/(2*q+1), 0, q/(2*q+1)};
            }
            else if (p1_geno == aa && p2_geno == aa)
            {
                mat_result = {0, (1-q)/(q+1),2*q/(q+1)};
                pat_result = {0, 0, 1};
            } 
            else 
            {
                Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
            }
        }
    } 
    else 
    {
        // shared father
        if (p1_sex == FEMALE && p2_sex == FEMALE)
        {
            if (p1_geno == AA && p2_geno == AA) 
            {
                mat_result = {p, q, 0};
                pat_result = {1, 0, 0};
            }
            else if (p1_geno == AA && p2_geno == Aa) 
            {
                mat_result = {p, q, 0};
                pat_result = {1, 0, 0};
            }
            else if (p1_geno == AA && p2_geno == aa) 
            {
                mat_result = {0, 0, 0};
                pat_result = {0, 0, 0};
            }
            else if (p1_geno == Aa && p2_geno == AA)
            {                
                mat_result = {0, p, q};
                pat_result = {1, 0, 0};
            }
            else if (p1_geno == Aa && p2_geno == Aa)
            {
                mat_result = {p*p, 2*p*q, q*q};
                pat_result = {q, 0, p};
            }
            else if (p1_geno == Aa && p2_geno == aa)
            {
                mat_result = {p, 0, q};
                pat_result = {0, 0, 1};
            }
            else if (p1_geno == aa && p2_geno == AA)
            {
                mat_result = {0, 0, 0};
                pat_result = {0, 0, 0};
            }
            else if (p1_geno == aa && p2_geno == Aa)
            {
                mat_result = {0, p, q};
                pat_result = {0, 0, 1};
            }
            else if (p1_geno == aa && p2_geno == aa)
            {
                mat_result = {0, p, q};
                pat_result = {0, 0, 1};
            }
            else 
            {
                Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
            }
        } 
        else if (p1_sex == MALE && p2_sex == MALE) 
        {
            mat_result = {0, 0, 0};
            pat_result = {0, 0, 0};
        } 
        else {
            // p1=male, p2=female
            if (p1_sex == MALE && p2_sex == FEMALE) 
            {
                if (p1_geno == AA && p2_geno == AA) 
                {
                    mat_result = {p, q, 0};
                    pat_result = {1, 0, 0};
                }
                else if (p1_geno == AA && p2_geno == Aa)
                {
                    mat_result = {p, q, 0};
                    pat_result = {0.5f, 0, 0.5f};
                }
                else if (p1_geno == AA && p2_geno == aa)
                {
                    mat_result = {p, q, 0};
                    pat_result = {0, 0, 1};
                }
                else if (p1_geno == aa && p2_geno == AA)
                {
                    mat_result = {0, p, q};
                    pat_result = {1, 0, 0};
                }
                else if (p1_geno == aa && p2_geno == Aa)
                {
                    mat_result = {0, p, q};
                    pat_result = {0.5f, 0, 0.5f};
                }
                else if (p1_geno == aa && p2_geno == aa)
                {
                    mat_result = {0, p, q};
                    pat_result = {0, 0, 1};
                }
                else 
                {
                    Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
                }
            } 
            else 
            {
                // p1=female, p2=male
                if ( p1_geno == AA && p2_geno == AA) 
                {
                    mat_result = {p, q, 0};
                    pat_result = {1, 0, 0};
                }
                else if (p1_geno == Aa && p2_geno == AA)
                {
                    mat_result = {p/2, 1/2.0f, q/2};
                    pat_result = {0.5f, 0, 0.5f};
                }
                else if (p1_geno == aa && p2_geno == AA)
                {
                    mat_result = {0, p, q};
                    pat_result = {1, 0, 0};
                }
                else if (p1_geno == AA && p2_geno == aa)
                {
                    mat_result = {p, q, 0};
                    pat_result = {1, 0, 0};
                }
                else if (p1_geno == Aa && p2_geno == aa)
                {
                    mat_result = {p/2, 1/2.0f, q/2};
                    pat_result = {0.5f, 0, 0.5f};
                }
                else if (p1_geno == aa && p2_geno == aa)
                {
                    mat_result = {0, p, q};
                    pat_result = {0, 0, 1};
                } 
                else 
                {
                    Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
                }
            }
        }
    }
    return make_pair(mat_result, pat_result);
}

/** 
 * Computes parental probabilities from user defined equations 
 * of full siblings xchromosome (same mother + father) 
 * p1_geno: individual 1 genotype: AA Aa or aa
 * p2_geno: individual 2 genotype: AA Aa or aa
 * p1_sex: gender of individual 1
 * p2_sex: gender of individual 2
 * maf: maximum allele frequency
 **/
pair<vector<float>, vector<float>> gcta::calc_prob_full_sib_Xchr(
    enum SNPGenotype p1_geno, enum SNPGenotype p2_geno, enum Gender p1_sex, enum Gender p2_sex, 
    float maf)
{
    vector<float> mat_result, pat_result;
    vector<string> mat_ids, pat_ids;
    //float p = maf;
    //float q = 1 - maf;
    _exp_parser.set_p(maf);
    _exp_parser.set_q(1-maf);

    if (p1_sex == FEMALE && p2_sex == MALE)
    {
        swap(p1_sex, p2_sex);
        swap(p1_geno, p2_geno);
    }

    if (p1_sex == MALE && p2_sex == MALE) 
    {
        if (p1_geno == AA && p2_geno == AA) 
        {
            mat_ids = {"PXf_mAA_mA_mA","PXf_mAa_mA_mA","PXf_maa_mA_mA"};
            //pat_ids = {"PXf_fA_mA_mA","","PXf_fa_mA_mA"};
        }
        else if ((p1_geno == AA && p2_geno == aa) || (p1_geno == aa && p2_geno == AA))
        {
            mat_ids = {"PXf_mAA_mA_ma","PXf_mAa_mA_ma","PXf_maa_mA_ma"};
            //pat_ids = {"PXf_fA_mA_ma","","PXf_fa_mA_ma"};
            //mat_result = {0, 1, 0};
        }
        else if (p1_geno == aa && p2_geno == aa)
        {
            mat_ids = {"PXf_mAA_ma_ma","PXf_mAa_ma_ma","PXf_maa_ma_ma"};
            //pat_ids = {"PXf_fA_ma_ma","","PXf_fa_ma_ma"};
        } 
        else {
            Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
        }
    }
    else if (p1_sex == MALE && p2_sex == FEMALE) 
    {
        //assumes p1 is will always be the male 
        if (p1_geno == AA && p2_geno == AA) 
        {
            mat_ids = {"PXf_mAA_mA_fAA","PXf_mAa_mA_fAA","PXf_maa_mA_fAA"};
            pat_ids = {"PXf_fA_mA_fAA","","PXf_fa_mA_fAA"};
        }
        else if (p1_geno == AA && p2_geno == Aa)
        {
            mat_ids = {"PXf_mAA_mA_fAa","PXf_mAa_mA_fAa","PXf_maa_mA_fAa"};
            pat_ids = {"PXf_fA_mA_fAa","","PXf_fa_mA_fAa"};
        }  
        else if (p1_geno == aa && p2_geno == AA) 
        {
            mat_ids = {"PXf_mAA_ma_fAA","PXf_mAa_ma_fAA","PXf_maa_ma_fAA"};
            pat_ids = {"PXf_fA_ma_fAA","","PXf_fa_ma_fAA"};
        }
        else if (p1_geno == AA && p2_geno == aa)
        {
            mat_ids = {"PXf_mAA_mA_faa","PXf_mAa_mA_faa","PXf_maa_mA_faa"};
            pat_ids = {"PXf_fA_mA_faa","","PXf_fa_mA_faa"};
        }
        else if (p1_geno == aa && p2_geno == aa)
        {
            mat_ids = {"PXf_mAA_ma_faa","PXf_mAa_ma_faa","PXf_maa_ma_faa"};
            pat_ids = {"PXf_fA_ma_faa", "","PXf_fa_ma_faa"};
        }
        else if (p1_geno == aa && p2_geno == Aa)
        {
            mat_ids = {"PXf_mAA_ma_fAa","PXf_mAa_ma_fAa","PXf_maa_ma_fAa"};
            pat_ids = {"PXf_fA_ma_fAa","","PXf_fa_ma_fAa"};
        } 
        else {
            Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
        }
    } 
    else if (p1_sex == FEMALE && p2_sex == FEMALE)
    {
        if (p1_geno == AA && p2_geno == AA)
        {
            mat_ids = {"PXf_mAA_fAA_fAA","PXf_mAa_fAA_fAA","PXf_maa_fAA_fAA"};
            pat_ids = {"PXf_fA_fAA_fAA", "","PXf_fa_fAA_fAA"};
        }
        else if (p1_geno == AA && p2_geno == Aa || p1_geno == Aa && p2_geno == AA)
        {
            mat_ids = {"PXf_mAA_fAA_fAa","PXf_mAa_fAA_fAa","PXf_maa_fAA_fAa"};
            pat_ids = {"PXf_fA_fAA_fAa","","PXf_fa_fAA_fAa"};
        }
        else if (p1_geno == Aa && p2_geno == Aa)
        {
            mat_ids = {"PXf_mAA_fAa_fAa","PXf_mAa_fAa_fAa","PXf_maa_fAa_fAa"};
            pat_ids = {"PXf_fA_fAa_fAa","","PXf_fa_fAa_fAa"};
        }
        else if (p1_geno == aa && p2_geno == aa)
        {
            mat_ids = {"PXf_mAA_faa_faa","PXf_mAa_faa_faa","PXf_maa_faa_faa"};            
            pat_ids = {"PXf_fA_faa_faa","","PXf_fa_faa_faa"};
        }
        else if (p1_geno == aa && p2_geno == Aa || p1_geno == Aa && p2_geno == aa)
        {
            mat_ids = {"PXf_mAA_faa_fAa","PXf_mAa_faa_fAa","PXf_maa_faa_fAa"};
            pat_ids = { "PXf_fA_faa_fAa","","PXf_fa_faa_fAa",};
        } 
        else 
        {
            Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
        }
    }
    else 
    {
        Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
    }

    mat_result = _exp_parser.get_probabilities(mat_ids);
    pat_result = _exp_parser.get_probabilities(pat_ids);

    /*for (int i = 0; i < mat_result.size(); i++)
    {
        cout << mat_result[i] << ",";
    }
    cout << endl;
    for (int i = 0; i < pat_result.size(); i++)
    {
        cout << pat_result[i] << ",";
    }
    cout << endl;*/

    return make_pair(mat_result, pat_result);
}

/** 
 * computes maternal probabilities from hard coded equations
 * Returns vector of length 3: P(Mum = AA| ...), P(Mum = Aa | ...), P(Mum = aa | ...)
 * p1_geno: individual 1 genotype: AA Aa or aa
 * p2_geno: individual 2 genotype: AA Aa or aa
 * p1_sex: gender of individual 1
 * p2_sex: gender of individual 2
 * maf: maximum allele frequency
 **/
pair<vector<float>, vector<float>> gcta::calc_prob_full_sib_Xchr_default(
    enum SNPGenotype p1_geno, enum SNPGenotype p2_geno, enum Gender p1_sex, enum Gender p2_sex, 
    float maf)
{
    vector<float> mat_result, pat_result;
    
    float p = maf;
    float q = 1 - maf;
    if (p1_sex == FEMALE && p2_sex == MALE)
    {
        swap(p1_sex, p2_sex);
        swap(p1_geno, p2_geno);
    }

    if (p1_sex == MALE && p2_sex == MALE) 
    {
        if (p1_geno == AA && p2_geno == AA) 
        {
            mat_result = {2*p/(p+1), (1-p)/(p+1), 0}; 
        }
        else if ((p1_geno == AA && p2_geno == aa) || (p1_geno == aa && p2_geno == AA))
        {
            mat_result = {0, 1, 0};
        }
        else if (p1_geno == aa && p2_geno == aa)
        {
            mat_result = {0, (1-q)/(q+1), 2*q/(q+1)};
        } 
        else {
            Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
        }
        //pat_result = mat_result;
    } 
    else if (p1_sex == MALE && p2_sex == FEMALE) 
    {
        //assumes p1 is will always be the male 
        if (p1_geno == AA && p2_geno == AA) 
        {
            mat_result = {2*p/(p+1), (1-p)/(p+1), 0};
            pat_result = {1, 0, 0};
        }
        else if (p1_geno == AA && p2_geno == Aa)
        {
            mat_result = {2*p/(2*p+1), 1/(2*p+1), 0};
            pat_result = {p/(2*p+1), 0, (p+1)/(2*p+1)};
        }  
        else if (p1_geno == aa && p2_geno == AA) 
        {
            mat_result = {0, 1, 0};
            pat_result = {1, 0, 0};
        }
        else if (p1_geno == AA && p2_geno == aa)
        {
            mat_result = {0, 1, 0};
            pat_result = {0, 0, 1};
        }
        else if (p1_geno == aa && p2_geno == aa)
        {
            mat_result = {0, (1-q)/(q+1), 2*q/(q+1)};
            pat_result = {0, 0, 1};
        }
        else if (p1_geno == aa && p2_geno == Aa)
        {
            mat_result = {0, 1/(2*q+1), 2*q/(2*q+1)};
            //mat_result = {0, 2*p/(2*p+1), 1/(2*p+1)};
            pat_result = {(q+1)/(2*q+1), 0, q/(2*q+1)};
        } 
        else {
            Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
        }
    } 
    else if (p1_sex == FEMALE && p2_sex == FEMALE)
    {
        if (p1_geno == AA && p2_geno == AA)
        {
            mat_result = {2*p/(p+1), (1-p)/(p+1), 0};
            pat_result = {1, 0, 0};
        }
        else if (p1_geno == AA && p2_geno == Aa || p1_geno == Aa && p2_geno == AA)
        {
            mat_result = {0, 1, 0};
            pat_result = {1, 0, 0};
        }
        else if (p1_geno == Aa && p2_geno == Aa)
        {
            mat_result = {2*p/3, 1/3.0f, 2*q/3};
            pat_result = {2/3.0f - p/3.0f, 0, 2/3.0f - q/3.0f};
        }
        else if (p1_geno == aa && p2_geno == aa)
        {
            mat_result = {0, (1-q)/(q+1), 2*q/(q+1)};            
            pat_result = {0, 0, 1};
        }
        else if (p1_geno == aa && p2_geno == Aa || p1_geno == Aa && p2_geno == aa)
        {
            mat_result = {0, 1, 0};
            pat_result = {0, 0, 1};
        } 

        else {
            Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
        }
    }
    else {
        Logger::getInstance().log_throw("Unknown Gender, should never reach here");
    }

    return make_pair(mat_result, pat_result);
}

/** 
 * Computes parental probabilities from user defined equations 
 * of full siblings (same mother + father) 
 * p1_geno: individual 1 genotype: AA Aa or aa
 * p2_geno: individual 2 genotype: AA Aa or aa
 * maf: maximum allele frequency
 */
vector<float> gcta::calc_parental_prob_full_sib(enum SNPGenotype p1_geno, 
    enum SNPGenotype p2_geno, float maf)
{
    vector<float> result;
    vector<string> ids; 

    _exp_parser.set_p(maf);
    _exp_parser.set_q(1-maf);

    if (p1_geno == INVALID || p2_geno == INVALID) 
    {
        return result;
    } 
    else if (p1_geno == AA && p2_geno == AA) 
    {
        ids = {"Pf_mAA_AA_AA", "Pf_mAa_AA_AA", "Pf_maa_AA_AA"};
    } 
    else if (p1_geno == AA &&  p2_geno == Aa) 
    {
        ids = {"Pf_mAA_AA_Aa", "Pf_mAa_AA_Aa", "Pf_maa_AA_Aa"};
    } 
    else if (p1_geno == Aa && p2_geno == AA) 
    {  
        ids = {"Pf_mAA_Aa_AA", "Pf_mAa_Aa_AA", "Pf_maa_Aa_AA"};
    } 
    else if (p1_geno == AA && p2_geno == aa) 
    {
        ids = {"Pf_mAA_AA_aa", "Pf_mAa_AA_aa", "Pf_maa_AA_aa"};
    } 
    else if (p1_geno == aa && p2_geno == AA) 
    {
        ids = {"Pf_mAA_aa_AA", "Pf_mAa_aa_AA", "Pf_maa_aa_AA"};
    } 
    else if (p1_geno == Aa && p2_geno == Aa) 
    {
        ids = {"Pf_mAA_Aa_Aa", "Pf_mAa_Aa_Aa", "Pf_maa_Aa_Aa"};
    } 
    else if (p1_geno == Aa && p2_geno == aa) 
    {
        ids = {"Pf_mAA_Aa_aa", "Pf_mAa_Aa_aa", "Pf_maa_Aa_aa"};
    } 
    else if (p1_geno == aa && p2_geno == Aa) 
    { 
        ids = {"Pf_mAA_aa_Aa", "Pf_mAa_aa_Aa", "Pf_maa_aa_Aa"};
    } 
    else if ((p1_geno == aa && p2_geno == aa)) 
    {
        ids = {"Pf_mAA_aa_aa", "Pf_mAa_aa_aa", "Pf_maa_aa_aa"};
    } 
    else 
    {
        Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
    }
    return _exp_parser.get_probabilities(ids);
}

/** 
 * Computes parental probabilities from user defined equations 
 * of half siblings (same mother + father)
 * p1_geno: individual 1 genotype: AA Aa or aa
 * p2_geno: individual 2 genotype: AA Aa or aa
 * maf: maximum allele frequency
 * same_mother: true if siblings share the same mother
 **/
vector<float> gcta::calc_parental_prob_half_sib(enum SNPGenotype p1_geno, 
    enum SNPGenotype p2_geno, float maf, bool same_mother)
{
    vector<float> result;
    vector<string> ids; 

    _exp_parser.set_p(maf);
    _exp_parser.set_q(1-maf);

    if (p1_geno == INVALID || p2_geno == INVALID) {
        return result;
    }

    if (same_mother) {
        if (p1_geno == AA && p2_geno == AA) 
        {
            ids = {"Phm_mAA_AA_AA", "Phm_mAa_AA_AA", "Phm_maa_AA_AA"};
        } 
        else if (p1_geno == AA && p2_geno == Aa) 
        {
            ids = {"Phm_mAA_AA_Aa", "Phm_mAa_AA_Aa", "Phm_maa_AA_Aa"};
        }  
        else if (p1_geno == Aa && p2_geno == AA)
        {
            ids = {"Phm_mAA_Aa_AA", "Phm_mAa_Aa_AA", "Phm_maa_Aa_AA"};
        }
        else if (p1_geno == AA && p2_geno == aa)
        {
            ids = {"Phm_mAA_AA_aa", "Phm_mAa_AA_aa", "Phm_maa_AA_aa"};
        }
        else if (p1_geno == aa && p2_geno == AA)
        {
            ids = {"Phm_mAA_aa_AA", "Phm_mAa_aa_AA", "Phm_maa_aa_AA"};
        }
        else if (p1_geno == Aa && p2_geno == Aa) 
        {
            ids = {"Phm_mAA_Aa_Aa", "Phm_mAa_Aa_Aa", "Phm_maa_Aa_Aa"};
        } 
        else if (p1_geno == Aa && p2_geno == aa)
        {
            ids = {"Phm_mAA_Aa_aa", "Phm_mAa_Aa_aa", "Phm_maa_Aa_aa"};
        }
        else if (p1_geno == aa && p2_geno == Aa)
        {
            ids = {"Phm_mAA_aa_Aa", "Phm_mAa_aa_Aa", "Phm_maa_aa_Aa"};
        }
        else if (p1_geno == aa && p2_geno == aa)
        {
            ids = {"Phm_mAA_aa_aa", "Phm_mAa_aa_aa", "Phm_maa_aa_aa"};
        } 
        else {
            Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
        }

    } else {
        if (p1_geno == Aa && p2_geno == Aa) {
            //hetreozygous case
            //float denom_p = 1+4*p-4*p*p;
            //float denom_q = 1+4*q-4*q*q;
            /*result = {
                (2*pow(p, 3)-pow(p, 4))/denom_p + (2*p*p-2*pow(p, 3))/denom_p + p*p*(1-p)*(1-p)/denom_p ,
                (2*p*p-2*pow(p, 3))/denom_p + 2*p*(1-p)/denom_p + (2*q*q-2*pow(q, 3))/denom_q, 
                (pow(p, 4)-6*p*p+4*p+1)/denom_p + (2*q*q-2*pow(q, 3))/denom_q + (2*pow(q, 3)-pow(q, 4))/denom_q          
            };*/
            ids = {"Phf_fAA_AA_Aa_Aa", "Phf_fAA_Aa_Aa_Aa", "Phf_fAA_aa_Aa_Aa"};
            result.push_back(sum_vec(_exp_parser.get_probabilities(ids)));

            ids = {"Phf_fAa_AA_Aa_Aa", "Phf_fAa_Aa_Aa_Aa", "Phf_fAa_aa_Aa_Aa"};
            result.push_back(sum_vec(_exp_parser.get_probabilities(ids)));

            ids = {"Phf_faa_AA_Aa_Aa", "Phf_faa_Aa_Aa_Aa", "Phf_faa_aa_Aa_Aa"};
            result.push_back(sum_vec(_exp_parser.get_probabilities(ids)));
            
            //cout << "[" << result[0] << "," << result[1] << "," << result[2] << "]" << endl; 
            return result; 
        } else {
            if (p1_geno == AA && p2_geno == AA) 
            {
                ids = {"Phf_fAA_AA_AA", "Phf_fAa_AA_AA", "Phf_faa_AA_AA"};
            }   
            else if (p1_geno == AA && p2_geno == Aa)
            {
                ids = {"Phf_fAA_AA_Aa", "Phf_fAa_AA_Aa", "Phf_faa_AA_Aa"};
            }
            else if (p1_geno == Aa && p2_geno == AA)
            {
                ids = {"Phf_fAA_Aa_AA", "Phf_fAa_Aa_AA", "Phf_faa_Aa_AA"};
            }
            else if (p1_geno == AA && p2_geno == aa)
            {
                ids = {"Phf_fAA_AA_aa", "Phf_fAa_AA_aa", "Phf_faa_AA_aa"};
            }
            else if (p1_geno == aa && p2_geno == AA)
            {
                ids = {"Phf_fAA_aa_AA", "Phf_fAa_aa_AA", "Phf_faa_aa_AA"};
            }
            else if (p1_geno == Aa && p2_geno == aa)
            {
                ids = {"Phf_fAA_Aa_aa", "Phf_fAa_Aa_aa", "Phf_faa_Aa_aa"};
            }
            else if (p1_geno == aa && p2_geno == Aa)
            {
                ids = {"Phf_fAA_aa_Aa", "Phf_fAa_aa_Aa", "Phf_faa_aa_Aa"};
            }
            else if (p1_geno == aa && p2_geno == aa)
            {
                ids = {"Phf_fAA_aa_aa", "Phf_fAa_aa_aa", "Phf_faa_aa_aa"};
            }
        }

    }
    return _exp_parser.get_probabilities(ids);

}

/**
 * Computes parental probabilities from hard coded equations 
 * of half siblings (same mother + father) 
 * p1_geno: individual 1 genotype: AA Aa or aa
 * p2_geno: individual 2 genotype: AA Aa or aa
 * maf: maximum allele frequency
 * same_mother: true if siblings share the same mother
 **/
pair<vector<float>, vector<float>> gcta::calc_parental_prob_half_sib_default(enum SNPGenotype p1_geno, 
    enum SNPGenotype p2_geno, float maf, bool same_mother)
{
    float p = maf;
    float q = 1 - maf;
    vector<float> result_mat, result_pat;
    pair<vector<float>, vector<float>> result;

    if (p1_geno == INVALID || p2_geno == INVALID) 
    {
        return result;
    }

    if (p1_geno == AA && p2_geno == AA) 
    {
        result_mat = {(2*p)/(p+1), (1-p)/(p+1), 0};
        result_pat = {p, q, 0};
    } 
    else if (p1_geno == AA && p2_geno == Aa) 
    {
        result_mat = {(2*p)/(2*p+1), 1/(2*p+1), 0};
        result_pat = {p, q, 0}; 
    } 
    else if (p1_geno == Aa && p2_geno == AA) 
    {
        result_mat = {(2*p)/(2*p+1), 1/(2*p+1), 0};
        result_pat = {p*p/(1+2*p), 2*p/(2*p+1), (1-p*p)/(2*p+1)};
    }
    else if (p1_geno == AA && p2_geno == aa)
    {
        result_mat = {0, 1, 0};
        result_pat = {p, q, 0};
    }
    else if (p1_geno == aa && p2_geno == AA) 
    {
        result_mat = {0, 1, 0};
        result_pat = {0, p, q}; 
    }
    else if (p1_geno == Aa && p2_geno == Aa) 
    {
        result_mat = {(2*p*(1-p))/(4*p*(1-p)+1), 1/(4*p*(1-p)+1), (2*q*(1-q))/(4*q*(1-q)+1)};
        
        float denom_p = 1+4*p-4*p*p;
        float denom_q = 1+4*q-4*q*q;

        result_pat = {
            (2*pow(p, 3)-pow(p, 4))/denom_p + (2*p*p-2*pow(p, 3))/denom_p + (p*p*(1-p)*(1-p))/denom_p ,
            (2*p*p-2*pow(p, 3))/denom_p + (2*p*(1-p))/denom_p + (2*q*q-2*pow(q, 3))/denom_q, 
            (p*p*(1-p)*(1-p))/denom_p + (2*q*q-2*pow(q, 3))/denom_q + (2*pow(q, 3)-pow(q, 4))/denom_q          
        };
    } 
    else if (p1_geno == Aa && p2_geno == aa) 
    {
        result_mat = {0, 1/(2*q+1), 2*q/(2*q+1)};
        result_pat = {p*(2-p)/(3-2*p), (2-2*p)/(3-2*p), (p-1)*(p-1)/(3-2*p)};
    }
    else if (p1_geno == aa && p2_geno == Aa)
    {
        result_mat = {0, 1/(2*q+1), 2*q/(2*q+1)};
        result_pat = {0, p, q}; 
    }
    else if (p1_geno == aa && p2_geno == aa)
    {
        result_mat = {0, (1-q)/(q+1), 2*q/(q+1)};
        result_pat = {0, p, q};
    } 
    else 
    {
        Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
    }
    return make_pair(result_mat, result_pat);
    
}

/**
 * Computes parental probabilities from hard coded equations 
 * of parent child duo (child with mother and missing father or child with father and missing mother) 
 * p1_geno: individual 1 genotype: AA Aa or aa
 * s1_geno: individual 2 genotype: AA Aa or aa
 * maf: maximum allele frequency
 **/
vector<float> gcta::calc_parental_prob_parent_duo(enum SNPGenotype p1_geno, enum SNPGenotype s1_geno, float maf)
{
    float p = maf;
    float q = 1 - maf;
    vector<float> result;

    if (p1_geno == INVALID || s1_geno == INVALID) 
    {
        return result;
    } 
    else if (p1_geno == AA && s1_geno == AA) 
    {
        result = {p, q, 0};
    }
    else if (p1_geno == AA && s1_geno == Aa)
    {
        result = {0, p, q};
    }
    else if (p1_geno == AA && s1_geno == aa)
    {
        result = {0, 0, 0};
    }
    else if (p1_geno == Aa && s1_geno == AA)
    {
        result = {p, q, 0};
    }
    else if (p1_geno == Aa && s1_geno == Aa)
    {
        result = {p*p/((p+q)*(p+q)), 2*p*q/((p+q)*(p+q)), q*q/((p+q)*(p+q))};
    }
    else if (p1_geno == Aa && s1_geno == aa)
    {
        result = {0, p, q};
    }
    else if (p1_geno == aa && s1_geno == AA)
    {
        result = {0, 0, 0};
    }
    else if (p1_geno == aa && s1_geno == Aa)
    {
        result = {p, q, 0};
    }
    else if (p1_geno == aa && s1_geno == aa)
    {
        result = {0, p, q};
    }
    else 
    {
        Logger::getInstance().log_throw("Unknown Genotype, should never reach here");
    }

    return result;
}

bool gcta::calc_known_parent_prob_snps(int p_father, int p_mother, eigenMatrix &probs_mat, 
    eigenMatrix &probs_pat)
{
    probs_mat.resize(1, _include.size());
    probs_pat.resize(1, _include.size());

    enum SNPGenotype pf_geno, pm_geno;
    bool ret = true;
    const vector<vector<float>> known_geno = {{0, 0, 1}, {0, 1, 0}, {0, 0, 0}, {1, 0, 0}};

    #pragma omp parallel for private(pf_geno, pm_geno) 
    for (int snp_i = 0; snp_i < _include.size(); snp_i++)
    {
        pf_geno = get_genotype(snp_i, p_father);
        pm_geno = get_genotype(snp_i, p_mother);
        
        if (pf_geno == INVALID) {
            Logger::getInstance() << "Warning: Invalid genotype for ";
            Logger::getInstance() << "FID:" << _fid[_keep[p_father]] << " IID:" << 
                _pid[_keep[p_father]] << " SNP:" << _snp_name[snp_i] << std::endl;
            ret = false;
        } 
        if (pm_geno == INVALID) {
            Logger::getInstance() << "Warning: Invalid genotype for ";
            Logger::getInstance() << "FID:" << _fid[_keep[p_mother]] << " IID:" << 
                _pid[_keep[p_mother]] << " SNP:" << _snp_name[snp_i] << std::endl;
            ret = false;
        }

        vector<float> result_pat = known_geno[pf_geno];
        probs_pat(0, snp_i) = result_pat.size() ? result_pat[1] + 2 * result_pat[2] : 0.0f;

        vector<float> result_mat = known_geno[pm_geno];
        probs_mat(0, snp_i) = result_mat.size() ? result_mat[1] + 2 * result_mat[2] : 0.0f;
    }
    
    return ret;
}

/**
 * Calculate probability for individuals with index child s1 and parent p1.
 * this works for parent-child duo 
 * s1: is the index of child
 * p1: is the index of parent
 * probs_mat: returned 1xSNP_NUM matrix containing imputed values
 * probs_pat: returned 1xSNP_NUM matrix containing imputed values
 * relationship: the relationship between the two siblings (halfsibs or fullsibs)
 **/
bool gcta::calc_missing_parent_prob_snps(int s1, int p1, eigenMatrix &probs_mat, 
    eigenMatrix &probs_pat, enum SibRel relationship)
{
    probs_mat.resize(1, _include.size());
    probs_pat.resize(1, _include.size());

    enum SNPGenotype s1_geno, p1_geno;
    bool ret = true;
    const vector<vector<float>> known_geno = {{0, 0, 1}, {0, 1, 0}, {0, 0, 0}, {1, 0, 0}};

    #pragma omp parallel for private(s1_geno, p1_geno) 
    for (int snp_i = 0; snp_i < _include.size(); snp_i++)
    {
        float maf = _maf[_include[snp_i]];
        s1_geno = get_genotype(_include[snp_i], s1);
        p1_geno = get_genotype(_include[snp_i], p1);

        // calculate probabilities
        vector<float> result_mat, result_pat;
        vector<float> result;

        try {
            if (_chr[_include[snp_i]] == 23) { 
                //xchr case -> need to consider gender
                enum Gender p1_sex, s1_sex;
                p1_sex = _sex[_keep[p1]] == 1 ? MALE : FEMALE;
                s1_sex = _sex[_keep[s1]] == 1 ? MALE : FEMALE;

                //todo: deal with X-chromosome case
            } else {
                //result_mat = calc_parental_prob_full_sib_default(p1_geno, p2_geno, 1-maf);
                //result_pat = {0, 0, 0}; 
                result = calc_parental_prob_parent_duo(p1_geno, s1_geno, maf);

            }
        } catch (...) {
            Logger::getInstance() << "Warning: Invalid genotype for ";
            Logger::getInstance() << "FID:" << _fid[_keep[s1]] << " IID:" << _pid[_keep[s1]] << 
                " SNP:" << _snp_name[snp_i] << std::endl;
            ret = false;            
        }

        enum SNPGenotype p1_geno = get_genotype(snp_i, p1);
        if (p1_geno == INVALID) {
            Logger::getInstance() << "Warning: Invalid genotype for ";
            Logger::getInstance() << "FID:" << _fid[_keep[p1]] << " IID:" << _pid[_keep[p1]] << 
                " SNP:" << _snp_name[snp_i] << std::endl;
            ret = false;
        }

        if (relationship == MOTHER_CHILD_DUO) {
            float val = result.size() ? result[1] + 2 * result[2] : 0.0f;
            probs_pat(0, snp_i) = val;

            // we know the mother's genotype so set to 1
            vector<float> result_mat = known_geno[p1_geno];
            probs_mat(0, snp_i) = result_mat.size() ? result_mat[1] + 2 * result_mat[2] : 0.0f;

        } else {
            float val = result.size() ? result[1] + 2 * result[2] : 0.0f;
            probs_mat(0, snp_i) = val;

            // we know the father's genotype so set to 1
            vector<float> result_pat = known_geno[p1_geno];
            probs_pat(0, snp_i) = result_pat.size() ? result_pat[1] + 2 * result_pat[2] : 0.0f;            
        }
    }

    return ret;
}

/**
 * Calculate probability for individuals with index p1 and sibling p2.
 * this works for half-sibs and full-sib pairs 
 * p1: is the index of individual 1
 * p2: is the index of individual 2
 * probs_mat: returned 1xSNP_NUM matrix containing imputed values
 * probs_pat: returned 1xSNP_NUM matrix containing imputed values
 * relationship: the relationship between the two siblings (halfsibs or fullsibs)
 * same_mother: true if the siblings share the same mother
 **/
bool gcta::calc_parental_prob_snps(int p1, int p2, eigenMatrix &probs_mat, eigenMatrix &probs_pat, 
    enum SibRel relationship, bool same_mother)
{
    probs_mat.resize(1, _include.size());
    probs_pat.resize(1, _include.size());

    enum SNPGenotype p1_geno, p2_geno;

    enum Gender p1_sex, p2_sex;
    bool ret = true;

    #pragma omp parallel for private(p1_geno, p2_geno, p1_sex, p2_sex) 
    for (int snp_i = 0; snp_i < _include.size(); snp_i++) 
    {
        float maf = _maf[_include[snp_i]];

        // todo: add comments here
        p1_geno = get_genotype(_include[snp_i], p1);
        p2_geno = get_genotype(_include[snp_i], p2);

        // calculate probabilities
        vector<float> result_mat, result_pat;
        pair<vector<float>, vector<float>> result;
        try {
            if (_chr[_include[snp_i]] == 23) { 
                //xchr case -> need to consider gender
                enum Gender p1_sex, p2_sex;
                p1_sex = _sex[_keep[p1]] == 1 ? MALE : FEMALE;
                p2_sex = _sex[_keep[p2]] == 1 ? MALE : FEMALE;
                
                if (relationship == FULL_SIB) {
                    //std::cout << _fid[_keep[p1]] << " " << _pid[_keep[p1]] << " " << _snp_name[snp_i] << std::endl;
                    if (_exp_parser.is_correct()) { 
                        // user specified equations
                        result = calc_prob_full_sib_Xchr(p1_geno, p2_geno, p1_sex, p2_sex, 1-maf);
                    } else {
                        // hardcoded equations
                        result = calc_prob_full_sib_Xchr_default(p1_geno, p2_geno, p1_sex, p2_sex, 1-maf);
                    }
                } else if (relationship == HALF_SIB) {
                    if (_exp_parser.is_correct()) {
                        // user specified equations
                        result = calc_prob_half_sib_Xchr(p1_geno, p2_geno, p1_sex, p2_sex, 1-maf, same_mother);
                    } else {
                        // hardcoded equations
                        result = calc_prob_half_sib_Xchr_default(p1_geno, p2_geno, p1_sex, p2_sex, 1-maf, same_mother);
                    }
                }
                
                result_mat = result.first;
                result_pat = result.second;
            } else {
                if (relationship == FULL_SIB) {
                    if (_exp_parser.is_correct()) { 
                        // user specified equations 
                        result_mat = calc_parental_prob_full_sib(p1_geno, p2_geno, 1-maf);
                        // maternal + paternal are the same for full sibs
                        result_pat = {0, 0, 0};
                    } else {
                        // hardcoded equations
                        result_mat = calc_parental_prob_full_sib_default(p1_geno, p2_geno, 1-maf);
                        result_pat = {0, 0, 0};
                    }
                } else if (relationship == HALF_SIB) {
                    if (_exp_parser.is_correct()) { 
                        // user specified equations 
                        result_mat = calc_parental_prob_half_sib(p1_geno, p2_geno, 1-maf, same_mother);
                        result_pat = {0, 0, 0};
                    } else {
                        // hardcoded equations
                        result = calc_parental_prob_half_sib_default(p1_geno, p2_geno, 1-maf, same_mother);
                        result_mat = result.first;
                        result_pat = result.second;
                    }            
                }
            }
        } catch (...) {
            Logger::getInstance() << "Warning: Invalid genotype for ";
            if (p1_geno == INVALID) {
                Logger::getInstance() << "FID:" << _fid[_keep[p1]] << " IID:" << _pid[_keep[p1]] << 
                    " SNP:" << _snp_name[snp_i] << std::endl;
            } else {
                Logger::getInstance() << "FID:" << _fid[_keep[p2]] << " IID:" << _pid[_keep[p2]] << 
                    " SNP:" << _snp_name[snp_i] << std::endl;
            }
            ret = false;
        }
        
        float val = result_mat.size() ? result_mat[1] + 2 * result_mat[2] : 0.0f;
        //fill up the corresponding column in matrix
        probs_mat(0, snp_i) = val;

        val = result_pat.size() ? result_pat[1] + 2 * result_pat[2] : 0.0f;
        probs_pat(0, snp_i) = val;

    }    
    return ret;
}

/**
 * Dump the genotypes out to a file for a particular SNP. For debugging only
 **/
void gcta::dump_genotypes(string filename, string snp)
{
    ofstream outfile;
    outfile.open(filename);

    if (_snp_name_map.find(snp) == _snp_name_map.end()) {
        Logger::getInstance().log_throw ("No such SNP found!");
    } 
    
    // get the index for the particular snp
    int i = _snp_name_map[snp];

    // get the genotypes for each individual
    for (int p = 0; p < _fid.size(); p++)
    {
        int p_geno = (int) get_genotype(i, p);
        outfile << p_geno << endl;
    }
    outfile.close();
}

/**
 * Parses maternal/paternal file 
 * imp_mat_eqn_file: file containing equations
 **/
void gcta::parse_parental_probability_file(string imp_mat_eqn_file)
{
    _exp_parser.parse_file(imp_mat_eqn_file);
}

/**
 * Dump the maternal probabilities out to a file for a particular SNP
 * is_maternal true to dump maternal genotypes otherwise dumps paternal genotypes
 * filename: file to dump to
 * is_maternal: true if maternal else false
 */
void gcta::dump_prob_to_file(string filename, bool is_maternal)
{
    ofstream outfile;
    outfile.open(filename);
    outfile << std::setprecision(3);

    /*Logger::getInstance() << "Writing dosages to file: " + filename << endl; 
    outfile << "fam_id:within_fam_id,";

    // first row -> list the column titles
    for (int i = 0; i < _include.size(); i++) 
    {
        outfile << _snp_name[_include[i]] + "_" + _allele1[i];
        if (i != _include.size()-1) {
            outfile << ",";
        } 
    }
    outfile << endl;
    outfile << std::setprecision(3);
    // remaining rows -> each individual genotype
    for (int row = 0; row < _keep.size(); row++)
    {
        outfile << _fid[_keep[row]] << ":" << _pid[_keep[row]] << ",";
        for (int col = 0; col < _include.size(); col++)
        {
            if (is_maternal)
                outfile << _mat_geno(_keep[row], col); 
            else
                outfile << _pat_geno(_keep[row], col);
            
            if (col != _include.size()-1) {
                outfile << ",";
            }
        }
        outfile << endl;
    }*/
    outfile << "SNP,";
    for (int col = 0; col < _keep.size(); col++)
    {
        outfile << _fid[_keep[col]] << ":" << _pid[_keep[col]];
        if (col != _keep.size() - 1) {
            outfile << ",";
        }
    } 
    outfile << endl;

    for (int row = 0; row < _include.size(); row++)
    {
        outfile << _snp_name[_include[row]] + "_" + _allele1[row] << ",";
        for (int col = 0; col < _keep.size(); col++)
        {
            if (is_maternal)
                outfile << _mat_geno(_keep[col], row);
            else
                outfile << _pat_geno(_keep[col], row);

            if (col != _keep.size()-1) {
                outfile << ",";
            }
        }
        outfile << endl;        
    }

    outfile.close();
}

/**
 * Dump the maternal probabilities out to the plink data format (format=1) 
 * is_maternal: true to dump maternal genotypes otherwise dumps paternal genotypes
 * halfsib: true if half siblings
 * filename: will output to filename.map, filename.fam, filename.dose
 */
void gcta::dump_prob_to_plink_dosage(string filename, bool is_maternal, bool halfsib)
{
    ofstream outfile;

    //write out .dose file
    outfile.open(filename + ".dose");
    outfile << std::setprecision(3);
    outfile << std::setw(10) << "SNP  ";
    outfile << "A1  " << "A2  ";

    Logger::getInstance() << endl;
    Logger::getInstance() << "Dumping " << (is_maternal ? "maternal" : "paternal") << 
        " dosages to plink data format" << endl;

    Logger::getInstance() << "Writing out " << filename + ".dose" << endl;

    map<string, int> seen;
    for (int col = 0; col < _keep.size(); col++)
    {
        string id;
        bool valid = false;
        if (!halfsib || is_maternal) {
            // either fullsibs or maternal (halfsibs)
            // mother id
            id = _fid[_keep[col]] + " " + _mo_id[_keep[col]];
            if (seen.find(id) == seen.end()) {
                seen[id] = col;
                outfile << id;
                valid = true;
            }
        }
        else {
            // father id
            id = _fid[_keep[col]] + " " + _fa_id[_keep[col]];
            if (seen.find(id) == seen.end()) {
                seen[id] = col;
                outfile << id;
                valid = true;
            }
        }

        if (col != _keep.size() - 1 && valid) {
            outfile << " ";
        }
    } 
    outfile << endl;


    for (int row = 0; row < _include.size(); row++)
    {
        outfile << _snp_name[_include[row]] + " " + _allele1[row] + " " + _allele2[row] + " ";
        seen.clear();
        
        for (int col = 0; col < _keep.size(); col++)
        {
            string id;
            bool valid = false;

            if (!halfsib || is_maternal) {
                id = _fid[_keep[col]] + " " + _mo_id[_keep[col]];
                
                if (seen.find(id) == seen.end()) {
                    valid = true;
                    seen[id] = col;
                    outfile << _mat_geno(_keep[col], row);
                }
            } else {
                id = _fid[_keep[col]] + " " + _fa_id[_keep[col]];

                if (seen.find(id) == seen.end()) {
                    valid = true;
                    seen[id] = col;
                    outfile << _pat_geno(_keep[col], row);
                }
            }
            if (col != _keep.size()-1 && valid) {
                outfile << " ";
            }
            
            
            /*if (seen.find(id) == seen.end() && is_maternal) {
                seen[id] = col;
                
                if (is_maternal)
                    outfile << _mat_geno(_keep[col], row);
                else
                    outfile << _pat_geno(_keep[col], row);
                
                if (col != _keep.size()-1) {
                    outfile << " ";
                }
            } 
            
            if (halfsib) {
                id = _fid[_keep[col]] + " " + _fa_id[_keep[col]];
                
                if (seen.find(id) == seen.end()) {
                    seen[id] = col;
                
                    if (is_maternal)
                        outfile << _mat_geno(_keep[col], row);
                    else
                        outfile << _pat_geno(_keep[col], row);                    
                    
                    if (col != _keep.size()-1) {
                        outfile << " ";
                    }                
                } 
            }*/
        }
        outfile << endl;        
    }    
    outfile.close();

    // write out .map file
    outfile.open(filename + ".map");
    Logger::getInstance() << "Writing out " << filename + ".map" << endl;
    
    for (int row = 0; row < _include.size(); row++)
    {
        outfile << _chr[_include[row]] << " " << _snp_name[_include[row]] << " " << 
            _genet_dst[_include[row]] << " " << _bp[_include[row]] << endl;            
    }
    outfile.close();

    // write out .fam file 
    outfile.open(filename + ".fam");
    Logger::getInstance() << "Writing out " << filename + ".fam" << endl;
    
    for (map<string, int>::const_iterator it = seen.begin(); it != seen.end(); it++)
    {
        string id = it->first;
        int row = it->second;

        outfile << id << " 0 0 0 0 " << endl;
    }
    outfile.close();
}


/**
 * Parse csv file of sibling relationships and validate
 * filename: file containing sibling relationships
 **/
void gcta::read_sibling_relationship_file(const string &filename)
{
    ifstream infile;
    infile.open(filename);
    string line;

    vector<bool> seen(_keep.size(), false);
    int count = 0;

    if (!infile.is_open())
        Logger::getInstance().log_throw("Error opening sibling relationship file: " + filename);
    
    Logger::getInstance() << "Reading relationships file " << filename << endl;
    //cout << "Reading relationships file " << filename << endl;
    while ( getline (infile,line))
    {
        count++;
        if (count <= 1 || line.size() == 0 || line[0] == '#') {
            continue;
        }
        // comma separated 5 items: fid1, pid1, fid2, pid2, relationship
        vector<string> csv = StringOps::split(line, ',');
        if (csv.size() != 5) {
            Logger::getInstance() << "Incorrect no. of values on line " << count << endl;
            break;
        } 
        string fid1, fid2, pid1, pid2, relationship;
        fid1 = csv[0];
        fid2 = csv[2];
        pid1 = csv[1];
        pid2 = csv[3];
        relationship = csv[4];

        string id1 = fid1 + ":" + pid1;
        string id2 = fid2 + ":" + pid2;

        // check see if fid and pid exists    
        map<string, int>::iterator iter = _id_map.find(id1);
        if (iter == _id_map.end()) {
            Logger::getInstance() << "Line " << count << ": Individual with fid1 " << fid1 << 
                " and id1 " << pid1 << " does not exist" << endl; 
            break;
        } 

        // repeat for sibling
        iter = _id_map.find(id2);
        if (iter == _id_map.end()) {
            Logger::getInstance() << "Line " << count << ": Individual with fid2 " << fid2 << 
                " and id2 " << pid2 << " does not exist\n";
            break;
        }

        // map letters to relationship: HF - Half sibling (share same father), 
        // HM - Half sibling (share same mother) , F -> Full sibling
        SibRel rel; 
        if (relationship == "HF" || relationship == "HM") {
            rel = HALF_SIB;
        } else if (relationship == "F") {
            rel = FULL_SIB;
        } else {
            Logger::getInstance() << "Line " << count << ": Neither Half (HM, HF) or Full (F) sibling\n";
            break;
        }

        int index1 = _id_map[id1];
        int index2 = _id_map[id2];

        // mark individual as having been seen
        seen[index1] = true;
        seen[index2] = true;
        vector<int> indices = {index1, index2};

        if (rel == HALF_SIB) {
            // check to see if either same father or same mother
            if (relationship[1] == 'F') {
                add_to_parent_map(_parent_sib_map, _fa_id[index1], indices, rel);
            } else {
                add_to_parent_map(_parent_sib_map, _mo_id[index1], indices, rel);
            }

        } else if (rel == FULL_SIB) {
            // both father and mother must be the same
            // use both mother and father as key (guaranteed to be unique)
            string parent_id = _fa_id[index1] + ":" + _mo_id[index1];

            add_to_parent_map(_parent_sib_map, parent_id, indices, rel);
        }

    }

    // individuals that were not found in csv, set to NO_SIB 
    for (int i = 0; i < seen.size(); i++) {
        if (!seen[i]) {
            string parent = _fa_id[i] + ":" + _mo_id[i];
            vector<int> indices = {i};
            _parent_sib_map[parent] = {indices, {}, NO_SIB};
        }
    }
    
    //cout << "Successfully read file" << endl;
    infile.close();       
}

/**
 * Tries to find all sibling relationships (full and half) in .fam, 
 * prioritises full sibs over half
 **/
void gcta::find_sibling_relationships()
{
    typedef unordered_map<string, vector<int>> map_vec_int_t;
    
    map_vec_int_t mothers, fathers, both;
    map_vec_int_t::iterator got;

    Logger::getInstance() << "\nFinding sibling relationships..." << endl;

    // full siblings have preference
    for (int i = 0; i < _keep.size(); i++)
    {
        if (!check_parental_id_valid(i)) 
        {
            continue;
        }

        string key = _fid[i] + ":" + _fa_id[i] + ":" + _mo_id[i];
        got = both.find(key);

        if (got == both.end())
        {
            both[key] = {i};
        } 
        else 
        {
            both[key].push_back(i);
        }
    }

    // used for marking processed individuals
    vector<int> seen(_keep.size(), false);
    int count = 0;

    //identify full sibling pairs
    for (auto p = both.begin(); p != both.end(); p++)
    {
        vector<int> indices = p->second; 
        
        // more than one individual with the same parent         
        if (indices.size() >= 2) 
        {
            SibRel_t sib_rel = {indices, {}, FULL_SIB};
            _parent_sib_map[p->first] = sib_rel;

            for (int i = 0; i < indices.size(); i++) {
                seen[indices[i]] = true;
            }

            count++;
        }
    }
    
    Logger::getInstance() << "Identified " << count << " full sibling(s)" << endl; 

    // now try half siblings
    for (int i = 0; i < _keep.size(); i++)
    {
        //we've have not identified the individual as a fullsib and their mother's id is valid
        if (!seen[i] && check_parental_id_valid(i, true)) {
            string key = _fid[i] + ":" + _mo_id[i];

            // check for same mother
            got = mothers.find(key);
            if (got == mothers.end()) {
                mothers[key] = {i};
            } else {
                mothers[key].push_back(i);
            }
        }

        //we've have not identified the individual as a fullsib and their father's id is valid
        if (!seen[i] && check_parental_id_valid(i, false)) {
            string key = _fid[i] + ":" + _fa_id[i];

            // check for same father
            got = fathers.find(key);
            if (got == fathers.end()) {
                fathers[key] = {i};
            } else {
                fathers[key].push_back(i);
            }
        }
    }

    int mother_duo_count = 0, father_duo_count = 0;
    int half_sib_mother = 0, half_sib_father = 0;
    int trio_count = 0;
    count = 0;

    // function to add halfsibs
    auto add_half_siblings = [&] (const map_vec_int_t &parent, bool same_mother)
    {
        //loop through found parents
        for (auto p = parent.begin(); p != parent.end(); p++)
        {
            vector<int> child_indices = p->second; 
            vector<int> parent_indices;

            // individuals with the same father or mother
            if (child_indices.size() == 2) 
            {
                SibRel relationship = HALF_SIB;
                int c1 = child_indices[0];
                int c2 = child_indices[1];
                
                if (same_mother) {
                    //1 x shared
                    int m = parent_indices[0];

                    bool m_avail = _id_map.find(_fid[m] + ":" + _pid[m]) != _id_map.end();
                    bool f1_avail = _id_map.find(_fid[c1] + ":" + _fa_id[c1]) != _id_map.end();
                    bool f2_avail = _id_map.find(_fid[c2] + ":" + _fa_id[c2]) != _id_map.end();
                    
                    if (m_avail && !f1_avail && !f2_avail) {
                        // 2 offspring half-siblings and 1 shared parent
                        relationship = HALF_SIB_SINGLE_SHARED_PARENT;
                    } else if (!m_avail && (f1_avail ^ f2_avail)) {
                        // 2 offspring half-siblings and 1 non-shared parents
                        relationship = HALF_SIB_SINGLE_NON_SHARED_PARENT;
                    } else if (m_avail && (f1_avail ^ f2_avail)) {
                        // 2 offspring half-siblings and 1 shared parent and 1 non-shared parents
                        relationship = HALF_SIB_SHARED_AND_NON_SHARED_PARENTS;
                    } else if (!m_avail && f1_avail && f2_avail) {
                        // 2 offspring half-siblings and 2 non-shared parents
                        relationship = HALF_SIB_NON_SHARED_PARENTS;
                    } 
                    half_sib_mother++;
                } else {
                    int f = parent_indices[0];

                    bool f_avail = _id_map.find(_fid[f] + ":" + _pid[f]) != _id_map.end();
                    bool m1_avail = _id_map.find(_fid[c1] + ":" + _mo_id[c1]) != _id_map.end();
                    bool m2_avail = _id_map.find(_fid[c2] + ":" + _mo_id[c2]) != _id_map.end();                    

                    if (f_avail && !m1_avail && !m2_avail) {
                        // 2 offspring half-siblings and 1 shared parent
                        relationship = HALF_SIB_SINGLE_SHARED_PARENT;
                    } else if (!f_avail && (m1_avail ^ m2_avail)) {
                        // 2 offspring half-siblings and 1 non-shared parents
                        relationship = HALF_SIB_SINGLE_NON_SHARED_PARENT;
                    } else if (f_avail && (m1_avail ^ m2_avail))  {
                        // 2 offspring half-siblings and 1 shared parent and 1 non-shared parents
                        relationship = HALF_SIB_SHARED_AND_NON_SHARED_PARENTS;
                    } else if (!f_avail && m1_avail && m2_avail) {
                        // 2 offspring half-siblings and 2 non-shared parents
                        relationship = HALF_SIB_NON_SHARED_PARENTS;
                    }              
                    half_sib_father++;
                }     

                SibRel_t sib_rel = {child_indices, parent_indices, relationship};
                _parent_sib_map[p->first] = sib_rel;

                for (int i = 0; i < child_indices.size(); i++) {
                    seen[child_indices[i]] = true;
                }
                count++;
            }
        }
    };

    add_half_siblings(mothers, true);
    add_half_siblings(fathers, false);

    Logger::getInstance() << "Identified " << count << " half sibling(s) pairs (" << 
        half_sib_mother << " with same mother / " << half_sib_father << " with same father)" << endl; 

    // check for parent child duo 
    for (int i = 0; i < _keep.size(); i++)
    {
        if (!seen[i]) {
            //cout << "DUO:" << _fid[i] << ":" << _pid[i] << endl;
            string father_key = _fid[i] + ":" + _fa_id[i];
            string mother_key = _fid[i] + ":" + _mo_id[i];

            if (_id_map.find(father_key) != _id_map.end() && _id_map.find(mother_key) == _id_map.end()) {
                // found father, but missing mother
                vector<int> child_indices = {i};
                vector<int> parent_indices = {_id_map[father_key]};
                SibRel_t sib_rel = {child_indices, parent_indices, FATHER_CHILD_DUO};

                _parent_sib_map[father_key] = sib_rel;
                //cout << "Father Child: " << _fa_id[i] << " " << _pid[i] << endl;
                father_duo_count++;
                seen[i] = true;

            } else if (_id_map.find(mother_key) != _id_map.end() && _id_map.find(father_key) == _id_map.end()) {
                // found mother, but missing father
                vector<int> child_indices = {i};
                vector<int> parent_indices = {_id_map[mother_key]};
                SibRel_t sib_rel = {child_indices, parent_indices, MOTHER_CHILD_DUO};

                _parent_sib_map[mother_key] = sib_rel; 
                mother_duo_count++;

                seen[i] = true;
            } else if (_id_map.find(mother_key) != _id_map.end() && _id_map.find(father_key) != _id_map.end()) {
                // found mother and father -> trio
                vector<int> child_indices = {i};
                vector<int> parent_indices = {_id_map[father_key], _id_map[mother_key]};
                SibRel_t sib_rel = {child_indices, parent_indices, TRIO};
                
                _parent_sib_map[mother_key] = sib_rel; 
                trio_count++;
            }
        }
    }

    Logger::getInstance() << "Identified " << mother_duo_count << " mother-child duo(s)" << endl;
    Logger::getInstance() << "Identified " << father_duo_count << " father-child duo(s)" << endl;
    Logger::getInstance() << "Identified " << trio_count << " trio(s)" << endl;


    // remaining individuals have no siblings
    count = 0;
    for (int i = 0; i < _keep.size(); i++)
    {
        if (!seen[i]) {
            string key = _fa_id[i] + ":" + _mo_id[i];
            _parent_sib_map[key] = {{i}, {}, NO_SIB};
            count++;
        }
    }

    Logger::getInstance() << "Identified " << count << " individual(s) with no siblings" << endl << endl;
}

/**
 * Main driver function to compute all maternal + paternal probabilities
 * dump_prob is true if user wishes to dump probabilities to a file
 * prob_file is the file to dump the probabilities to
 * halfsib is true if halfsibs should be imputed, will be set to True if parent-duo found
 * xchr flag is set if there are snps residing in the xchromosome
 * debug_dosage_format: true to dump to plain csv otherwise defaults to the plink data format
 **/
void gcta::generate_parental_probabilities(bool dump_prob, string prob_file, bool &halfsib, 
    bool force_fullsib, bool &xchr, bool debug_dosage_format)
{
    if (_mu.empty())
    {
        calcu_mu();
        calcu_maf();
    }
 
    if (_parent_sib_map.empty()) {
        // try to identify relationships (user did not specify relationships)
        find_sibling_relationships();
    }

    vector<string> keep_ids;
    //vector<int> valid_ids;
    int missing = 0, parental_count = 0, mother_duo_count = 0, father_duo_count = 0, trio_count = 0, errors = 0; 
    bool duo = false;
    vector<string> valid_snps;

    xchr = false;

    // check snps for xchromosome
    for (int i = 0; i < _include.size(); i++) {
        if (_chr[_include[i]] == 23) {
            xchr = true;
            break;
        } 
    }
    
    // No. of individuals x No. of SNPs
    _mat_geno.resize(_keep.size(), _include.size());
    _pat_geno.resize(_keep.size(), _include.size());
    _relationships.resize(_keep.size());

    for (auto p = _parent_sib_map.begin(); p != _parent_sib_map.end(); p++)
    {
        vector<int> child_indices = p->second.children; 
        vector<int> parent_indices = p->second.parent;

        eigenMatrix prob_mat, prob_pat;
        
        SibRel relationship = p->second.relationship;
        int p1, p2;

        if ( relationship == FULL_SIB && (!halfsib || duo)) 
        {
            p1 = child_indices[0];
            p2 = child_indices[1];

            // only check gender if xchromosome snp
            if (xchr && (!check_gender_valid(p1) || !check_gender_valid(p2)))
                continue;

            bool ret = calc_parental_prob_snps(p1, p2, prob_mat, prob_pat, relationship, true);
            
            // update siblings with same probability
            for (int i = 0; i < child_indices.size(); i++)
            {
                _mat_geno.block(child_indices[i], 0, 1, _include.size()) = prob_mat;
                _pat_geno.block(child_indices[i], 0, 1, _include.size()) = prob_pat;
            }
            if (ret) {
                string indi1 = _fid[p1] + ":" + _pid[p1];
                string indi2 = _fid[p2] + ":" + _pid[p2];
                // store away the siblings we have seen
                keep_ids.push_back(indi1);
                keep_ids.push_back(indi2); 

                _relationships[p1] = relationship;
                _relationships[p2] = relationship;

                parental_count++;
                //Logger::getInstance() << indi1 << " " <<  indi2 << " Inputing parental genotypes (full-sibs)" << endl;
            } else {
                errors++;
            }
        }
        else if ( relationship == HALF_SIB && halfsib )
        {
            p1 = child_indices[0];
            p2 = child_indices[1];
            bool same_mother = _mo_id[p1] == _mo_id[p2];

            // only check gender if xchromosome snp
            if (xchr && (!check_gender_valid(p1) || !check_gender_valid(p2)))
                continue;
            
            bool ret = calc_parental_prob_snps(p1, p2, prob_mat, prob_pat, relationship, same_mother);

            if (ret) 
            {
                if (same_mother)
                {
                    _mat_geno.block(p1, 0, 1, _include.size()) = prob_mat;
                    _mat_geno.block(p2, 0, 1, _include.size()) = prob_mat;

                    _pat_geno.block(p1, 0, 1, _include.size()) = prob_pat;

                    //flip ordering to find paternal probabilities for p2
                    ret = calc_parental_prob_snps(p2, p1, prob_mat, prob_pat, relationship, same_mother);
                    _pat_geno.block(p2, 0, 1, _include.size()) = prob_pat;
                } 
                else {
                    _pat_geno.block(p1, 0, 1, _include.size()) = prob_pat;
                    _pat_geno.block(p2, 0, 1, _include.size()) = prob_pat;

                    _mat_geno.block(p1, 0, 1, _include.size()) = prob_mat;

                    //flip ordering to find paternal probabilities for p2
                    ret = calc_parental_prob_snps(p2, p1, prob_mat, prob_pat, relationship, same_mother);
                    _mat_geno.block(p2, 0, 1, _include.size()) = prob_mat;
                }
            }
            if (ret) 
            {
                string indi1 = _fid[p1] + ":" + _pid[p1];
                string indi2 = _fid[p2] + ":" + _pid[p2];                
                // store away the siblings we have seen
                keep_ids.push_back(indi1);
                keep_ids.push_back(indi2);

                _relationships[p1] = relationship;
                _relationships[p2] = relationship;
                //Logger::getInstance() << indi1 << " " <<  indi2 << " Inputing parental genotypes (half-sibs)" << endl;
                parental_count++;
            } else {
                errors++;
            }
        } 
        else if ( relationship == MOTHER_CHILD_DUO || relationship == FATHER_CHILD_DUO )
        {   
            p1 = parent_indices[0];
            p2 = child_indices[0];

            bool ret = calc_missing_parent_prob_snps(p2, p1, prob_mat, prob_pat, relationship);
            _mat_geno.block(p2, 0, 1, _include.size()) = prob_mat;
            _pat_geno.block(p2, 0, 1, _include.size()) = prob_pat;

            if (ret) {
                //string indi_child = _fid[p2] + ":" + _pid[p2];
                //string indi_parent = _fid[p1] + ":" + _pid[p1];

                keep_ids.push_back(_fid[p2] + ":" + _pid[p2]);
                _relationships[p2] = relationship;  

                if (relationship == MOTHER_CHILD_DUO) {
                    mother_duo_count++;
                    //Logger::getInstance() << indi_child << " " << indi_parent << " Imputing paternal genotypes " << endl; 
                } else { 
                    father_duo_count++;
                    //Logger::getInstance() << indi_child << " " << indi_parent << " Imputing maternal genotypes " << endl; 
                }
                //Logger::getInstance() << indi1 << " " <<  indi2 << " Inputing parental genotypes (half-sibs)" << endl;          
            } else {
                errors++;
            }
            if (!force_fullsib)
                halfsib = true;
            duo = true;
        } 
        else if ( relationship == TRIO ) 
        {
            p1 = parent_indices[0]; //father
            p2 = parent_indices[1]; //mother
            int c = child_indices[0]; //child 

            bool ret = calc_known_parent_prob_snps(p1, p2, prob_mat, prob_pat);
            _pat_geno.block(p1, 0, 1, _include.size()) = prob_pat;
            _mat_geno.block(p2, 0, 1, _include.size()) = prob_mat;

            if (ret) {
                keep_ids.push_back(_fid[c] + ":" + _pid[c]);
                _relationships[c] = relationship;
                trio_count++;
            } else {
                errors++;
            }

            if (!force_fullsib) 
                halfsib = true;
        }
        else if ( relationship == HALF_SIB_NON_SHARED_PARENTS || relationship == HALF_SIB_SHARED_AND_NON_SHARED_PARENTS 
            || relationship == HALF_SIB_SINGLE_NON_SHARED_PARENT || relationship == HALF_SIB_SINGLE_SHARED_PARENT) 
        {
            p1 = parent_indices[0];
            int s1 = child_indices[0];            
            int s2 = child_indices[1];

        }
        else {
            // person has no siblings -> individual will be removed anyway   
            prob_mat = eigenMatrix::Zero(1, _include.size());
            prob_pat = eigenMatrix::Zero(1, _include.size());

            string relationship_str = "";
            if (relationship == FULL_SIB)
                relationship_str = "full sib";
            else if (relationship == HALF_SIB)
                relationship_str = "half sib";
            else if (relationship == MOTHER_CHILD_DUO)
                relationship_str = "mother child duo";
            else if (relationship == FATHER_CHILD_DUO)
                relationship_str = "father child duo";
            else
                relationship_str = "no sibs";

            // save probabilities for each sibling
            for (int i = 0; i < child_indices.size(); i++ ) 
            {
                _mat_geno.block(child_indices[i], 0, 1, _include.size()) = prob_mat;
                _pat_geno.block(child_indices[i], 0, 1, _include.size()) = prob_pat;
                Logger::getInstance() << "Removing individual with fid:" + _fid[child_indices[i]] + " and id:" + _pid[child_indices[i]] <<
                    " (" + relationship_str + ")" << endl;
            }
        }
    }
    Logger::getInstance() << endl;

    if (force_fullsib && (mother_duo_count || father_duo_count)) {
        parental_count += mother_duo_count + father_duo_count;
        mother_duo_count = 0;
        father_duo_count = 0;
        Logger::getInstance() << "Forcing parental genotype imputations(s) (full-sibs)" << endl;
    }

    Logger::getInstance() << "Performed " << parental_count << " parental genotype imputation(s) (full-sibs/half-sibs)" << endl;
    Logger::getInstance() << "Performed " << mother_duo_count  << " paternal imputation(s) (mother-child duos)" << endl;
    Logger::getInstance() << "Performed " << father_duo_count  << " maternal imputation(s) (father-child duos)" << endl;
    Logger::getInstance() << "Encountered " << errors << " error(s)" << endl;

    Logger::getInstance() << endl;

    // removes those individuals with no siblings
    update_id_map_kp(keep_ids, _id_map, _keep);

    // dump to file for debugging
    if (dump_prob) 
    {     
        Logger::getInstance() << endl;
        if (debug_dosage_format)
            dump_prob_to_file(prob_file + "_mat.txt", true);
        else 
            dump_prob_to_plink_dosage(prob_file + "_mat", true, halfsib);
            
        if (halfsib || xchr) {
            if (debug_dosage_format)
                dump_prob_to_file(prob_file + "_pat.txt", false);
            else           
            {
                if (xchr)
                    halfsib = true;
                dump_prob_to_plink_dosage(prob_file + "_pat", false, halfsib);
            }
        }
    }
}