#full siblings
#e.g. Pf_mAA_Aa_AA = P(Mum=AA | Sib=Aa, Sib=AA)
   
Pf_mAA_AA_AA := (2*p)/(p+1)
Pf_mAa_AA_AA := (1-p)/(p+1)
Pf_maa_AA_AA := 0
   
Pf_mAA_AA_Aa := p/(p+1)
Pf_mAa_AA_Aa := 1/(p+1)
Pf_maa_AA_Aa := 0
   
Pf_mAA_AA_aa := 0
Pf_mAa_AA_aa := 1
Pf_maa_AA_aa := 0

Pf_mAA_Aa_AA := Pf_mAA_AA_Aa
Pf_mAa_Aa_AA := Pf_mAa_AA_Aa
Pf_maa_Aa_AA := Pf_maa_AA_Aa

Pf_mAA_Aa_Aa := (p-0.5*p^2)/(1+p-p^2)
Pf_mAa_Aa_Aa := 0.5/(1+p-p^2)
Pf_maa_Aa_Aa := (0.5-0.5*p^2)/(1+p-p^2)

Pf_mAA_Aa_aa := 0
Pf_mAa_Aa_aa := 1/(q+1)
Pf_maa_Aa_aa := q/(q+1)

Pf_mAA_aa_AA := Pf_mAA_AA_aa
Pf_mAa_aa_AA := Pf_mAa_AA_aa
Pf_maa_aa_AA := Pf_maa_AA_aa

Pf_mAA_aa_Aa := Pf_mAA_Aa_aa
Pf_mAa_aa_Aa := Pf_mAa_Aa_aa
Pf_maa_aa_Aa := Pf_maa_Aa_aa

Pf_mAA_aa_aa := 0
Pf_mAa_aa_aa := (1-q)/(q+1)
Pf_maa_aa_aa := (2*q)/(q+1)


#half siblings - share same mother
Phm_mAA_AA_AA := 2*p/(p+1)
Phm_mAa_AA_AA := (1-p)/(p+1)
Phm_maa_AA_AA := 0

Phm_mAA_AA_Aa := 2*p/(2*p+1)
Phm_mAa_AA_Aa := 1/(2*p+1)
Phm_maa_AA_Aa := 0

Phm_mAA_Aa_AA := Phm_mAA_AA_Aa  
Phm_mAa_Aa_AA := Phm_mAa_AA_Aa
Phm_maa_Aa_AA := Phm_maa_AA_Aa

Phm_mAA_Aa_Aa := 2*p*(1-p)/(4*p*(1-p)+1)
Phm_mAa_Aa_Aa := 1/(4*p*(1-p)+1)
Phm_maa_Aa_Aa := 2*q*(1-q)/(4*q*(1-q)+1)

Phm_mAA_AA_aa := 0
Phm_mAa_AA_aa := 1
Phm_maa_AA_aa := 0

Phm_mAA_aa_AA := Phm_mAA_AA_aa
Phm_mAa_aa_AA := Phm_mAa_AA_aa 
Phm_maa_aa_AA := Phm_maa_AA_aa

Phm_mAA_Aa_aa := 0
Phm_mAa_Aa_aa := 1/(2*q+1)
Phm_maa_Aa_aa := 2*q/(2*q+1) 

Phm_mAA_aa_Aa := Phm_mAA_Aa_aa
Phm_mAa_aa_Aa := Phm_mAa_Aa_aa
Phm_maa_aa_Aa := Phm_maa_Aa_aa

Phm_mAA_aa_aa := 0
Phm_mAa_aa_aa := (1-q)/(q+1)
Phm_maa_aa_aa := 2*q/(q+1)

#half siblings - share same father (homozygous)
Phf_fAA_AA_AA := p
Phf_fAa_AA_AA := q
Phf_faa_AA_AA := 0

Phf_fAA_AA_Aa := p
Phf_fAa_AA_Aa := q
Phf_faa_AA_Aa := 0

Phf_fAA_Aa_AA := p*p/(1+2*p) 
Phf_fAa_Aa_AA := 2*p/(1+2*p)
Phf_faa_Aa_AA := (1-p*p)/(1+2*p)

Phf_fAA_AA_aa := p
Phf_fAa_AA_aa := q
Phf_faa_AA_aa := 0

Phf_fAA_aa_AA := 0
Phf_fAa_aa_AA := p 
Phf_faa_aa_AA := q

Phf_fAA_Aa_aa := p*(2-p)/(3-2*p)
Phf_fAa_Aa_aa := (2-2*p)/(3-2*p)
Phf_faa_Aa_aa := ((1-p)^2)/(3-2*p)

Phf_fAA_aa_Aa := 0
Phf_fAa_aa_Aa := p
Phf_faa_aa_Aa := q

Phf_fAA_aa_aa := 0
Phf_fAa_aa_aa := p
Phf_faa_aa_aa := q

#half siblings - share same father (hetreozygous)
Phf_fAA_AA_Aa_Aa := (2*p^3-p^4)/(1+4*p-4*p^2)
Phf_fAA_Aa_Aa_Aa := (2*p^2-2*p^3)/(1+4*p-4*p^2)
Phf_fAa_AA_Aa_Aa := Phf_fAA_Aa_Aa_Aa
Phf_fAA_aa_Aa_Aa := ((p^2)*(1-p)^2)/(1+4*p-4*p^2)
Phf_faa_AA_Aa_Aa := Phf_fAA_aa_Aa_Aa
Phf_fAa_Aa_Aa_Aa := (2*p*(1-p))/(1+4*p-4*p^2)
Phf_faa_Aa_Aa_Aa := (2*q^2-2*q^3)/(1+4*q-4*q^2)
Phf_fAa_aa_Aa_Aa := Phf_faa_Aa_Aa_Aa
Phf_faa_aa_Aa_Aa := (2*q^3-q^4)/(1+4*q-4*q^2)


#full siblings X-Chromosome pairs male-female
PXf_mAA_mA_fAA := 2*p/(p+1)
PXf_mAa_mA_fAA := (1-p)/(p+1)
PXf_maa_mA_fAA := 0

PXf_mAA_mA_fAa := 2*p/(2*p+1)
PXf_mAa_mA_fAa := 1/(2*p+1)
PXf_maa_mA_fAa := 0

PXf_mAA_mA_faa := 0
PXf_mAa_mA_faa := 1
PXf_maa_mA_faa := 0

PXf_mAA_ma_fAA := 0
PXf_mAa_ma_fAA := 1
PXf_maa_ma_fAA := 0

PXf_mAA_ma_fAa := 0
PXf_mAa_ma_fAa := 1/(2*q+1)
PXf_maa_ma_fAa := 2*q/(2*q+1)

PXf_mAA_ma_faa := 0
PXf_mAa_ma_faa := (1-q)/(q+1)
PXf_maa_ma_faa := 2*q/(q+1)

PXf_fA_mA_fAA := 1
PXf_fa_mA_fAA := 0

PXf_fA_mA_fAa := p/(2*p+1)
PXf_fa_mA_fAa := (p+1)/(2*p+1)

PXf_fA_mA_faa := 0  
PXf_fa_mA_faa := 1

PXf_fA_ma_fAA := 1
PXf_fa_ma_fAA := 0

PXf_fA_ma_fAa := (q+1)/(2*q+1) 
PXf_fa_ma_fAa := q/(2*q+1)

PXf_fA_ma_faa := 0
PXf_fa_ma_faa := 1

#full siblings X-Chromosome pairs female-female
PXf_mAA_fAA_fAA := 2*p/(p+1)
PXf_mAa_fAA_fAA := (1-p)/(p+1)
PXf_maa_fAA_fAA := 0

PXf_mAA_fAA_fAa := 0
PXf_mAa_fAA_fAa := 1
PXf_maa_fAA_fAa := 0

PXf_mAA_fAa_fAa := 2*p/3 
PXf_mAa_fAa_fAa := 1/3.0
PXf_maa_fAa_fAa := 2*q/3

PXf_mAA_faa_fAa := 0
PXf_mAa_faa_fAa := 1
PXf_maa_faa_fAa := 0

PXf_mAA_faa_faa := 0
PXf_mAa_faa_faa := (1-q)/(q+1)
PXf_maa_faa_faa := 2*q/(q+1)

PXf_fA_fAA_fAA := 1
PXf_fa_fAA_fAA := 0

PXf_fA_fAA_fAa := 1
PXf_fa_fAA_fAa := 0

PXf_fA_fAa_fAa := 2/3.0 - p/3
PXf_fa_fAa_fAa := 2/3.0 - q/3

PXf_fA_faa_fAa := 0
PXf_fa_faa_fAa := 1

PXf_fA_faa_faa := 0
PXf_fa_faa_faa := 1


#full siblings X-Chromosome pairs male-male
#shared mother
PXf_mAA_mA_mA := 2*p/(p+1)
PXf_mAa_mA_mA := (1-p)/(p+1)
PXf_maa_mA_mA := 0

PXf_mAA_mA_ma := 0
PXf_mAa_mA_ma := 1
PXf_maa_mA_ma := 0

PXf_mAA_ma_ma := 0
PXf_mAa_ma_ma := (1-q)/(q+1)
PXf_maa_ma_ma := 2*q/(q+1)

PXf_fA_mA_mA := PXf_mAA_mA_mA
PXf_fa_mA_mA := PXf_maa_mA_mA

PXf_fA_mA_ma := PXf_mAA_mA_ma 
PXf_fa_mA_ma := PXf_maa_mA_ma

PXf_fA_ma_ma := PXf_mAA_ma_ma
PXf_fa_ma_ma := PXf_maa_ma_ma


#half siblings X-Chromosome pairs male-female
#shared mother
PXhm_mAA_mA_fAA := 2*p/(p+1)
PXhm_mAa_mA_fAA := (1-p)/(p+1)
PXhm_maa_mA_fAA := 0
     
PXhm_mAA_mA_fAa := 2*p/(2*p+1)
PXhm_mAa_mA_fAa := 1/(2*p+1)
PXhm_maa_mA_fAa := 0
    
PXhm_mAA_ma_fAA := 0
PXhm_mAa_ma_fAA := 1
PXhm_maa_ma_fAA := 0
     
PXhm_mAA_mA_faa := 0
PXhm_mAa_mA_faa := 1
PXhm_maa_mA_faa := 0

PXhm_mAA_ma_fAa := 0
PXhm_mAa_ma_fAa := 1/(2*q+1)
PXhm_maa_ma_fAa := 2*q/(2*q+1)

PXhm_mAA_ma_faa := 0
PXhm_mAa_ma_faa := (1-q)/(q+1)
PXhm_maa_ma_faa := 2*q/(q+1)  
    
PXhm_fAA_mA_fAA := 1
PXhm_faa_mA_fAA := 0
    
PXhm_fAA_mA_fAa := p/(2*p+1)
PXhm_faa_mA_fAa := (p+1)/(2*p+1)
    
PXhm_fAA_ma_fAA := 1
PXhm_faa_ma_fAA := 0

PXhm_fAA_mA_faa := 0
PXhm_faa_mA_faa := 1

PXhm_fAA_ma_fAa := (q+1)/(2*q+1)
PXhm_faa_ma_fAa := q/(2*q+1)

PXhm_fAA_ma_faa := 0
PXhm_faa_ma_faa := 1


#shared father
PXhf_mAA_mA_fAA := p
PXhf_mAa_mA_fAA := q
PXhf_maa_mA_fAA := 0

PXhf_mAA_mA_fAa := p
PXhf_mAa_mA_fAa := q
PXhf_maa_mA_fAa := 0

PXhf_mAA_ma_fAA := 0
PXhf_mAa_ma_fAA := p
PXhf_maa_ma_fAA := q

PXhf_mAA_mA_faa := p
PXhf_mAa_mA_faa := q
PXhf_maa_mA_faa := 0

PXhf_mAA_ma_fAa := 0
PXhf_mAa_ma_fAa := p
PXhf_maa_ma_fAa := q

PXhf_mAA_ma_faa := 0
PXhf_mAa_ma_faa := p
PXhf_maa_ma_faa := q      

PXhf_mAA_fAA_mA := p
PXhf_mAa_fAA_mA := q
PXhf_maa_fAA_mA := 0

PXhf_mAA_fAa_mA := p/2
PXhf_mAa_fAa_mA := 1/2.0
PXhf_maa_fAa_mA := q/2

PXhf_mAA_fAA_ma := p 
PXhf_mAa_fAA_ma := q
PXhf_maa_fAA_ma := 0

PXhf_mAA_faa_mA := 0
PXhf_mAa_faa_mA := p
PXhf_maa_faa_mA := q

PXhf_mAA_fAa_ma := p/2
PXhf_mAa_fAa_ma := 1/2.0
PXhf_maa_fAa_ma := q/2

PXhf_mAA_faa_ma := 0
PXhf_mAa_faa_ma := p
PXhf_maa_faa_ma := q


PXhf_fAA_mA_fAA := 1
PXhf_faa_mA_fAA := 0

PXhf_fAA_mA_fAa := 0.5
PXhf_faa_mA_fAa := 0.5

PXhf_fAA_ma_fAA := 1
PXhf_faa_ma_fAA := 0

PXhf_fAA_mA_faa := 0
PXhf_faa_mA_faa := 1

PXhf_fAA_ma_fAa := 0.5
PXhf_faa_ma_fAa := 0.5

PXhf_fAA_ma_faa := 0
PXhf_faa_ma_faa := 1


#half siblings X-Chromosome pairs female-female
#shared mother
PXhm_mAA_fAA_fAA := 2*p/(p+1)
PXhm_mAa_fAA_fAA := (1-p)/(p+1)
PXhm_maa_fAA_fAA := 0

PXhm_mAA_fAA_fAa := 2*p/(2*p+1)
PXhm_mAa_fAA_fAa := 1/(2*p+1)
PXhm_maa_fAA_fAa := 0

PXhm_mAA_fAa_fAA := 2*p/(2*p+1)
PXhm_mAa_fAa_fAA := 1/(2*p+1)
PXhm_maa_fAa_fAA := 0

PXhm_mAA_fAa_fAa := 2*p*(1-p)/(1+4*p-4*p^2)
PXhm_mAa_fAa_fAa := 1/(1+4*p-4*p^2)
PXhm_maa_fAa_fAa := 2*p*(1-p)/(1+4*p-4*p^2)

PXhm_mAA_faa_fAA := 0
PXhm_mAa_faa_fAA := 1
PXhm_maa_faa_fAA := 0

PXhm_mAA_fAA_faa := 0
PXhm_mAa_fAA_faa := 1
PXhm_maa_fAA_faa := 0

PXhm_mAA_faa_fAa := 0
PXhm_mAa_faa_fAa := 1/(2*q+1)
PXhm_maa_faa_fAa := 2*q/(2*q+1)

PXhm_mAA_fAa_faa := 0
PXhm_mAa_fAa_faa := 1/(2*q+1)
PXhm_maa_fAa_faa := 2*q/(2*q+1)

PXhm_mAA_faa_faa := 0
PXhm_mAa_faa_faa := (1-q)/(q+1)
PXhm_maa_faa_faa := 2*q/(q+1)

PXhm_fAA_fAA_fAA := 1
PXhm_faa_fAA_fAA := 0

PXhm_fAA_fAA_fAa := 1
PXhm_faa_fAA_fAa := 0

PXhm_fAA_fAa_fAA := p/(2*p+1)
PXhm_faa_fAa_fAA := (p+1)/(2*p+1)

PXhm_fAA_fAA_fAa_fAa := p*(2-p)/(1+4*p-4*p^2)
PXhm_fAA_faa_fAa_fAa := p*(1-p)/(1+4*p-4*p^2)
PXhm_faa_fAA_fAa_fAa := q*(1-q)/(1+4*q-4*q^2)
PXhm_faa_faa_fAa_fAa := q*(2-q)/(1+4*q-4*q^2)

PXhm_fAA_faa_fAA := 0
PXhm_faa_faa_fAA := 1

PXhm_fAA_fAA_faa := 1
PXhm_faa_fAA_faa := 0

PXhm_fAA_faa_fAa := 0
PXhm_faa_faa_fAa := 1

PXhm_fAA_fAa_faa := (q+1)/(2*q+1)
PXhm_faa_fAa_faa := q/(2*q+1)

PXhm_fAA_faa_faa := 0
PXhm_faa_faa_faa := 1


#shared father
PXhf_mAA_fAA_fAA := p
PXhf_mAa_fAA_fAA := q
PXhf_maa_fAA_fAA := 0

PXhf_mAA_fAA_fAa := p
PXhf_mAa_fAA_fAa := q
PXhf_maa_fAA_fAa := 0

PXhf_mAA_fAa_fAA := 0
PXhf_mAa_fAa_fAA := p
PXhf_maa_fAa_fAA := q

PXhf_mAA_fAa_fAa := p^2
PXhf_mAa_fAa_fAa := 2*p*q
PXhf_maa_fAa_fAa := q^2

PXhf_mAA_faa_fAA := 0
PXhf_mAa_faa_fAA := 0
PXhf_maa_faa_fAA := 0

PXhf_mAA_fAA_faa := 0
PXhf_mAa_fAA_faa := 0
PXhf_maa_fAA_faa := 0

PXhf_mAA_faa_fAa := 0
PXhf_mAa_faa_fAa := p
PXhf_maa_faa_fAa := q

PXhf_mAA_fAa_faa := p
PXhf_mAa_fAa_faa := 0
PXhf_maa_fAa_faa := q

PXhf_mAA_faa_faa := 0
PXhf_mAa_faa_faa := p
PXhf_maa_faa_faa := q

PXhf_fAA_fAA_fAA := 1
PXhf_faa_fAA_fAA := 0

PXhf_fAA_fAA_fAa := 1
PXhf_faa_fAA_fAa := 0

PXhf_fAA_fAa_fAA := 1
PXhf_faa_fAa_fAA := 0

PXhf_fAA_fAa_fAa := q
PXhf_faa_fAa_fAa := p

PXhf_fAA_faa_fAA := 0
PXhf_faa_faa_fAA := 0

PXhf_fAA_fAA_faa := 0
PXhf_faa_fAA_faa := 0

PXhf_fAA_faa_fAa := 0
PXhf_faa_faa_fAa := 1

PXhf_fAA_fAa_faa := 0
PXhf_faa_fAa_faa := 1

PXhf_fAA_faa_faa := 0
PXhf_faa_faa_faa := 1


#half siblings X-Chromosome pairs male-male
#shared mother      
PXhm_mAA_mA_mA := PXf_mAA_mA_mA
PXhm_mAa_mA_mA := PXf_mAa_mA_mA
PXhm_maa_mA_mA := PXf_maa_mA_mA

PXhm_mAA_mA_ma := PXf_mAA_mA_ma
PXhm_mAa_mA_ma := PXf_mAa_mA_ma
PXhm_maa_mA_ma := PXf_maa_mA_ma

PXhm_mAA_ma_mA := PXf_mAA_mA_ma
PXhm_mAa_ma_mA := PXf_mAa_mA_ma
PXhm_maa_ma_mA := PXf_maa_mA_ma

PXhm_mAA_ma_ma := PXf_mAA_ma_ma
PXhm_mAa_ma_ma := PXf_mAa_ma_ma
PXhm_maa_ma_ma := PXf_maa_ma_ma
